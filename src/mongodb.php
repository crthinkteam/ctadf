<?php

$container['mongo'] = function ($c) {
    $conn = (new MongoDB\Client('mongodb://'.getenv('MONGODB_HOST')))->t;
	return $conn;
};
