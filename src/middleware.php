<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

use Psr\Http\Message\ServerRequestInterface;
use CTAF\Model\UserRole;

$app->add(new \Slim\Middleware\Session([
    'name' => 'ctaf',
    'autorefresh' => true,
    'lifetime' => '20 minutes'
]));

$app->add(function (ServerRequestInterface $request, $response, $next) {
    /** @var \Slim\Router $router */
    $route = $request->getAttribute('route');
    if(empty($route)){
        $route_name = "/error404";
    } else {
        $route_name = $route->getName();
    }
    $no_auth = array(
        RouteRegistry::LOGIN_EXTERNAL_P,
//        RouteRegistry::TEMPLATE_CMS,
//        RouteRegistry::TEMPLATE_CMS_WORKING,
//        RouteRegistry::TEMPLATE_CMS_QAID,
//        RouteRegistry::TEMPLATE_CMS_QAID_TEST_RENDER,
//        RouteRegistry::TEMPLATE_CMS_QAID_TEST_RENDER_WIDE,
//        RouteRegistry::TEMPLATE_LOGIN,
//        RouteRegistry::SAML2_PAGE,
//        RouteRegistry::SAML2_INFO,
//        RouteRegistry::SAML2_TEST_LOGIN,
//        RouteRegistry::SAML2_TEST_LOGOUT,
//        RouteRegistry::SAML2_METADATA,
//        RouteRegistry::SAML2_ACS,
//        RouteRegistry::SAML2_SLS,
//        RouteRegistry::SSO_LOGIN,
//        RouteRegistry::SSO_LOGOUT,
        RouteRegistry::ROOT,
        RouteRegistry::LOGIN,
        RouteRegistry::LOGOUT,
        RouteRegistry::LOGIN_EXTERNAL,
        RouteRegistry::ERROR404,
    );
    if (in_array($route_name, $no_auth) || $this->session->exists(SessionKeys::USER_BM)) {
        $response = $next($request, $response);
        return $response;
    } else {
        $this->flash->addMessage(RouteRegistry::LOGIN, "Please log in.");
        return $response->withRedirect(RouteRegistry::LOGIN);
    }
});

/**
 *
 */

$app->add(function (ServerRequestInterface $request, $response, $next) {
    if (!$this->session->exists(SessionKeys::USER_BM)) {
        $response = $next($request, $response);
        return $response;
    }
    $user = $this->session->get(SessionKeys::USER_BM);
    if ($request->getMethod() === 'POST') {
        if (!$user->inRole(UserRole::ADMIN) && !$user->inRole(UserRole::PSR)) {
            $this->flash->addMessage(RouteRegistry::HOME, "Tsk tsk. No cookies for you.");
            return $response->withRedirect(RouteRegistry::HOME);
        }
    }
    $response = $next($request, $response);
    return $response;
});

$app->add(function (ServerRequestInterface $request, $response, $next) {
    /** @var \Slim\Router $router */
    $route = $request->getAttribute('route');
    if(empty($route)){
        // Its a 404, there is no route
        $route_name = "/error404";
    } else {
        $route_name = $route->getName();
    }
    $admin_only = array(
//        RouteRegistry::CMS_MANAGE,
//        RouteRegistry::ALTCOUNTRIES,
//        RouteRegistry::ALTINDUSTRIES,
//        RouteRegistry::DBSTATS,
//        RouteRegistry::DBSTATSJSON,
//        RouteRegistry::DBSTATSDL,
        RouteRegistry::PROFILE_TYPE_CREATE,
        RouteRegistry::PROFILE_TYPE_VIEW__code,
        RouteRegistry::PROFILE_TYPE_SAVE,
        RouteRegistry::PROFILE_TYPE_UPDATE,
//        RouteRegistry::BUILDER_VIEW__profile_type,
//        RouteRegistry::BUILDER_SAVE,
        RouteRegistry::USER_NEW,
        RouteRegistry::USER_NEW_P,
        RouteRegistry::USERS,
        RouteRegistry::USER_EDIT,
        RouteRegistry::USER_EDIT_P,
    );
    if (!$this->session->exists(SessionKeys::USER_BM)) {
        $response = $next($request, $response);
        return $response;
    }
    $user = $this->session->get(SessionKeys::USER_BM);
    if (in_array($route_name, $admin_only)) {
        if ($user->inRole(UserRole::ADMIN)) {
            $response = $next($request, $response);
            return $response;
        } else {
            $this->flash->addMessage(RouteRegistry::HOME, "Thous shall not pass.");
            return $response->withRedirect(RouteRegistry::HOME);
        }
    }
    $response = $next($request, $response);
    return $response;
});
