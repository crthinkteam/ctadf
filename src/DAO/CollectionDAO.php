<?php

declare(strict_types=1);

namespace CTAF\DAO;

use JsonSchema\Validator;
use MongoDB\BSON\ObjectId;
use MongoDB\Database;
use MongoDB\Collection;
use MongoDB\Client;
use MongoDB\Exception\RuntimeException;
use MongoDB\Exception\Exception;


abstract class CollectionDAO
{
    const CONNECTION_MODE_PROD = 'prod';
    const CONNECTION_MODE_TEST = 'test';

    const _ID = '_id';

    /**
     * @var string Collection_name
     */
    private $_collection_name;

    /**
     * @var Collection
     */
    private $_collection;

    /**
     * @var Database
     */
    private $_db;

    /**
     * @var $_schema
     */
    private $_schema;

    protected function collection(): Collection
    {
        return $this->_collection;
    }

    public function UUID()
    {
        $uuid = $this->_db->command(array('eval' => 'return tojson(UUID());'))->toArray()[0]['retval'];
        $pcs = explode('"', $uuid);
        return $pcs[1];
    }

    public function __construct($collection_name, $mode = CollectionDAO::CONNECTION_MODE_PROD)
    {
        // Save away this instances collection name.

        $this->_collection_name = trim($collection_name);

        $this->_schema = null;

        if ($mode == static::CONNECTION_MODE_TEST) {
            $db = getenv('MONGODB_TEST_DATABASE_NAME');
        } else {
            $db = getenv('MONGODB_DATABASE_NAME');
        }
        if (empty($db)) {
            throw new RuntimeException('Database name is empty.');
        }


        $host = getenv('MONGODB_HOST');
        if (empty($host)) {
            throw new RuntimeException('Host name is empty.');
        }


        $replicaset = getenv('MONGODB_REPLICASET');
        if (empty($replicaset)) {
            $replicaset = '';
        }

        $ssl = getenv('MONGODB_SSL');
        if (empty($ssl)) {
            $ssl = false;
        } else {
            $value = trim(strtolower($ssl));
            $ssl = false;
            switch ($value) {
                case 'yes':
                case'true':
                case '1':
                case 'on':
                    $ssl = true;
                    break;
            }
        }

        $user = getenv('MONGODB_USER');
        if (empty($user)) {
            $user = '';
        } else {
            $user = trim(strtolower($user));
        }

        $pass = getenv('MONGODB_PASSWD');
        if (empty($pass)) {
            $pass = '';
        } else {
            $pass = trim($pass);
        }

        if (strpos(',', $host) !== 0) {
            $hosts = explode(',', $host);
        } else {
            $hosts = [$host];
        }

        $connectstring = '';

        if (!empty($replicaset)) {
            $connectstring .= ($replicaset . "/");
        }

        foreach ($hosts as $host) {
            if (!empty($connectstring)) {
                $connectstring .= ',';
            }
            if (!empty($user)) {
                $userstring = $user;
                if (!empty($pass)) {
                    $userstring .= ":";
                    $userstring .= $pass;
                }
                $connectstring .= $userstring;
                $connectstring .= '@';
            }
            if (!empty($host)) {
                $connectstring .= $host;
            } else {
                $connectstring .= 'localhost:27017';
            }
        }
        $connectstring .= '/?';

        if ($ssl) {
            $connectstring .= 'ssl=true';
        } else {
            $connectstring .= 'ssl=false';
        }

        $connectstring = 'mongodb://' . $connectstring;

        $client = new Client($connectstring);
        $this->_db = $client->selectDatabase($db);
        try {
            $this->_collection = $client->selectDatabase($db)->selectCollection($collection_name);
        } catch (Exception $e) {
            $this->_collection = $client->selectDatabase($db)->createCollection($collection_name);
        }
    }

    /**
     * Save one document in the collection. Return the _id as a string.
     *
     * @param $data
     * @return string
     */
    public function insertOne($data): string
    {
        $result = $this->_collection->insertOne($data);
        $insertId = $result->getInsertedId();
        return (string)$insertId;
    }

    /**
     * Save many documents in a collection.
     * @param $data
     * @return array
     */
    public function insertMany($data): array
    {
        $result = $this->_collection->insertMany($data);
        $insertIds = $result->getInsertedIds();
        $retval = array();
        foreach ($insertIds as $id) {
            $retval[] = (string)$id;
        }
        return $retval;
    }

    /**
     * Always return a result. Return an empty array when nothing's found.
     *
     * @param $id
     * @return array
     */
    public function getOne(string $id): array
    {
        $result = $this->_collection->findOne([static::_ID => new ObjectId($id)]);
        if (empty($result)) {
            return [];
        }
        // flatten the _id field
        $result[static::_ID] = $result[static::_ID]->__toString();
        return (array)$result;
    }

    public function deleteOne($id): int
    {
        $deleteResult = $this->_collection->deleteOne([static::_ID => new ObjectId($id)]);
        return $deleteResult->getDeletedCount();
    }

    public function findBy($filter, int $limit = 0): array
    {
        $lists = [];
        $cursor = [];
        if (!empty($filter)) {
            if ($limit > 0) {
                $cursor = $this->_collection->find($filter, ['limit' => $limit]);
            } else {
                $cursor = $this->_collection->find($filter);
            }
        } else {
            $cursor = $this->_collection->find([]);
        }
        foreach ($cursor as $the) {
            if (isset($the[static::_ID])) {
                $the[static::_ID] = $the[static::_ID]->__toString();
            }
            array_push($lists, (array)$the);
        }
        return $lists;
    }

    public function findByCursor($filter, int $limit = 0): array
    {
        $lists = [];
        $cursor = [];
        if ($limit > 0) {
            $cursor = $this->_collection->find($filter, ['limit' => $limit]);
        } else {
            $cursor = $this->_collection->find($filter);
        }
        return $cursor;
    }

    public function findByTypeMap($filter, $options = []): array
    {
        $options['typeMap'] = ['root' => 'array', 'document' => 'array', 'array' => 'array'];
        $lists = [];
        $cursor = $this->_collection->find($filter, $options);
        foreach ($cursor as $the) {
            if (isset($the[static::_ID])) {
                $the[static::_ID] = $the[static::_ID]->__toString();
            }
            array_push($lists, (array)$the);
        }
        return $lists;
    }

    public function findOption($filter,$options): array
    {
        $options['typeMap'] = ['root' => 'array', 'document' => 'object', 'array' => 'array'];
        $lists = [];
        $cursor = $this->_collection->find($filter, $options);
        foreach ($cursor as $the) {
            if (isset($the[static::_ID])) {
                $the[static::_ID] = $the[static::_ID]->__toString();
            }
            array_push($lists, (array)$the);
        }
        return $lists;
    }

    public function findByObject($filter): array
    {
        $options['typeMap'] = ['root' => 'array', 'document' => 'object', 'array' => 'array'];
        $lists = [];
        $cursor = $this->_collection->find($filter, $options);
        foreach ($cursor as $the) {
            if (isset($the[static::_ID])) {
                $the[static::_ID] = $the[static::_ID]->__toString();
            }
            array_push($lists, (array)$the);
        }
        return $lists;
    }

    public function updateOne($entity): int
    {
        $id = $entity[static::_ID];
        unset($entity[static::_ID]);
        $updateResult = $this->_collection->replaceOne([static::_ID => new ObjectId($id)], $entity);
        return $updateResult->getModifiedCount();
    }

    protected function all()
    {
        return $this->findBy([]);
    }

    protected function allTypeMap()
    {
        return $this->findByTypeMap([]);
    }


    /** TODO: The following generic stats functions should all use a '$exists'=>$feildname
     * as either part of the query or as a $match stage in the pipeline, to ensure that records that dont
     * have the correct control feilds are not counted.
     */


    public function getCount($query = [])
    {
        if (empty($query)) {
            $count = $this->collection()->count();
        } else {
            $count = $this->collection()->count($query);
        }
        return $count;
    }


    public function getStats($field = "profile_type", $addTotal = false, $unwind = [], $project = [], $default = "Unknown")
    {
        $pipeline = [
        ];
        if (!empty($unwind)) {
            foreach ($unwind as $field) {
                $pipeline[] = [
                    '$unwind' => [
                        'path' => '$' . $field
                    ]
                ];
            }
        }
        if (!empty($project)) {
            foreach ($project as $field => $source) {
                $pipeline[] = [
                    '$project' => [
                        $field => '$' . $source
                    ]
                ];
            }
        }
        $pipeline[] =
            [
                '$group' => [
                    "_id" => [
                        $field => '$' . $field
                    ],
                    "count" => ['$sum' => 1]
                ]
            ];

        $pipeline[] =
            [
                '$sort' => [
                    'count' => -1
                ]
            ];

        $pipeline[] =
            [
                '$out' => ('profilestats-' . $field)
            ];
        $results = $this->collection()->aggregate($pipeline, ['allowDiskUse' => true]);

        $totalcount = $this->getCount();

        $profileCollection = $this->_db->selectCollection('profilestats-' . $field);

        $hResults = $profileCollection->find([]);

        $data = [];

        $total = 0;

        foreach ($hResults as $result) {
            $percent = intval(floatval($result['count'] * 10000) / $totalcount);
            $value = $result['_id'][$field];
            if (empty($value)) {
                $value = $default;
            }

            $total += $result['count'];

            $data[] = [
                'value' => $value,
                'count' => $result['count'],
                'percentage' => $percent / 100
            ];

        }
        if ($addTotal) {
            $data[] = ['total' => $total];
        }
        return $data;

    }


    public function getAgeStats($field = "createdAt", $financial = true, $addTotal = false, $unwind = [], $project = [], $default = "Unknown")
    {
        $totalcount = $this->getCount();

        $pipeline = [
        ];
        if (!empty($unwind)) {
            foreach ($unwind as $field) {
                $pipeline[] = [
                    '$unwind' => [
                        'path' => '$' . $field
                    ]
                ];
            }
        }
        if (!empty($project)) {
            foreach ($project as $field => $source) {
                $pipeline[] = [
                    '$project' => [
                        $field => '$' . $source
                    ]
                ];
            }
        }


        $pipeline[] =
            ['$group' => [
                '_id' => [
                    'year' => ['$year' => '$' . $field],
                    'q' => [
                        '$ceil' => [

                            '$divide' => [
                                ['$month' => '$' . $field],
                                3
                            ]

                        ]
                    ]
                ],
                'count' => [
                    '$sum' => 1
                ]
            ]];

        $pipeline[] =
            ['$sort' => [
                '_id' => 1
            ]
            ];

        $pipeline[] =
            [
                '$project' => [
                    'count' => 1,
                    "percentage" => [
                        '$multiply' => [
                            [
                                '$divide' => [
                                    '$count',
                                    $totalcount
                                ]
                            ],
                            100
                        ]
                    ],
                ]

            ];

        $pipeline[] =
            [
                '$out' => ('profileagestats-' . $field)
            ];

        $results = $this->collection()->aggregate($pipeline, ['allowDiskUse' => true]);

        $profileCollection = $this->_db->selectCollection('profileagestats-' . $field);

        $hResults = $profileCollection->find([]);

        $data = [];

        $total = 0;

        foreach ($hResults as $result) {
            $percent = intval($result['percentage'] * 100);

            $quarter = $result['_id']['q'];
            $year = $result['_id']['year'];

            if ($financial) {
                $quarter -= 1;
                if ($quarter <= 0) {
                    $quarter = 4;
                    $year--;
                }
            }

            $value = $year . " Q" . $quarter;

            if (empty($value)) {
                $value = $default;
            }

            $total += $result['count'];


            $data[] = [
                'value' => $value,
                'count' => $result['count'],
                'percentage' => $percent / 100
            ];

        }

        if ($addTotal) {
            $data[] = ['total' => $total];
        }
        return $data;

    }

    public function validateSchemaBy($filter)
    {
        if (empty($filter)) {
            throw new RuntimeException('expecting a mongodb query array');
        }

        if (!is_array($filter)) {
            throw new RuntimeException('expecting a mongodb query array');
        }
        $data = $this->findBy($filter);
        if (empty($data[0])) {
            throw new RuntimeException('objectId does not refference a valid object');
        }

        return $this->validateSchemaData($data[0]);
    }

    public function get_nested_array_value($array, $path, $delimiter = '/')
    {
        $pathParts = explode($delimiter, $path);
        $current = $array;
        foreach ($pathParts as $key) {
            if ($key !== "") {
                if (is_object($current)) {
                    $current = $current->$key;
                } else {
                    $current = $current[$key];
                }
            }
        }
        return $current;
    }


    /**
     * @param null $data_in
     * @return array|null
     * @throws RuntimeException
     * @throws \JsonSchema\Exception\InvalidArgumentException
     */

    public function validateSchemaData($data_in = null)
    {
        $data = null;
        if (is_array($data_in)) {
            $data = Validator::arrayToObjectRecursive($data_in);
        } else if (is_object($data_in)) {
            $data = $data_in;
        } else {
            throw new RuntimeException("Expecting either an array or an object as input");
        }
        if (empty($this->_schema)) {
            // we have not loaded the schema yet
            $absRoot = __DIR__ . "/";
            $schemaFile = $absRoot . "../" . "json-schema/schema-" . $this->_collection_name . ".json";
            $realSchemaFile = realpath($schemaFile);

            if (empty($realSchemaFile)) {
                throw new RuntimeException("schema file [$schemaFile] does not exist");
            }

            $schemaData = file_get_contents($realSchemaFile);

            if (empty($schemaData)) {
                throw new RuntimeException("schema file [$realSchemaFile] is empty");
            }

            $this->_schema = json_decode($schemaData);

            if (empty($this->_schema)) {
                $error = json_last_error_msg();

                throw new RuntimeException("invalid schema file used in validation: [{$realSchemaFile}]");

            }
        }

        $validator = new Validator();
        $validator->validate(Validator::arrayToObjectRecursive($data),
            $this->_schema,
            \JsonSchema\Constraints\Constraint::CHECK_MODE_VALIDATE_SCHEMA
        );

        if ($validator->isValid()) {
            return null;
        } else {
            $validationResult = $validator->getErrors();

            // add an "element' feild to the validation error so we can see what caused the problem.

            if (!empty($validationResult)) {
                foreach ($validationResult as &$__validationResult) {
                    switch ($__validationResult['constraint']) {
                        case 'type':
                        case 'required':
                        case 'enum':
                            if (isset($__validationResult['pointer'])) {
                                $value = $this->get_nested_array_value($data, $__validationResult['pointer']);
                                if (!empty($value)) {
                                    $__validationResult['element'] = $value;
                                } else {
                                    $__validationResult['element'] = "[Empty]";
                                }
                            }
                            break;
                    }
                }
            }

            $cleanedViolations = $this->replaceKey($validationResult, [
                '$secs' => "__secs",
                '$id' => '__id',
                '$date' => '__date',
                '$data' => '__data',
                '$numberLong' => "__numberLong"
            ]);

            return $cleanedViolations;
        }
    }

    public function logSchemaViolations($id, $violations)
    {
        $hSchemalog = SchemalogDAO::getInstance();
        $hSchemalog->log('schema-' . $this->_collection_name, $id, $violations);
    }

    public function replaceKey($original, $map = array())
    {
        /** Create a temp var encoding object or array to json */
        $temp = json_encode($original);
        /** Loopint to replace keys on json */
        foreach ($map AS $k => $v) {
            $temp = str_ireplace('"' . $k . '":', '"' . $v . '":', $temp);
        }
        /** Default return is array format but if the original is a object return it in object */
        $array = true;
        if (is_object($original)) {
            $array = false;
        }
        return json_decode($temp, $array);
    }


}
