<?php

namespace BFD\DAO;

use BFD\Model\ProfileTypeBM;
use BFD\Model\ProfileTypeBMArray;

class ProfileTypeDAO extends CollectionDAO
{
    //SchemaV2 enums
    const PROFILE_TYPE_CORP = 'Corporate';
    const PROFILE_TYPE_INDIV = 'Individual';

    public function __construct($mode = 'prod')
    {
        parent::__construct('profile_types', $mode);

    }

    public function all(): ProfileTypeBMArray
    {
        $retval = new ProfileTypeBMArray();
        $all = parent::all();
        foreach ($all as $pt) {
            $profile_t = new ProfileTypeBM();
            $profile_t->profile_type = $pt[ProfileTypeBM::PROFILE_TYPE];
            $profile_t->name = $pt[ProfileTypeBM::NAME];
            $retval->add($profile_t);
        }
        return $retval;
    }

    public function getProfileType(string $code): ProfileTypeBM
    {
        $result = parent::findBy([ProfileTypeBM::PROFILE_TYPE => $code])[0];
        if (empty($result)) {
            return new ProfileTypeBM(true);
        }
        $profile_t = new ProfileTypeBM();
        $profile_t->profile_type = $result[ProfileTypeBM::PROFILE_TYPE];
        $profile_t->name = $result[ProfileTypeBM::NAME];
        return $profile_t;
    }

}
