<?php

namespace CTAF\DAO;

use MongoDB\BSON\ObjectId;

/**
 * LookupDao
 */
class LookupDAO extends CollectionDAO
{

    /**
     * construct method
     */
    public function __construct($mode = 'prod')
    {
        parent::__construct('lookup', $mode);
    }

    public function getLookup($lookupid)
    {
        // TODO: use find one
        $result = parent::findByTypeMap(['_id' => new ObjectId($lookupid)]);
        if ($result === null) {
            throw new \Exception("Lookup id '$lookupid' not found.");
        }
        $result = $result[0];
        return $result;
    }

    public function getLookupList($set)
    {
        $result = parent::findOption(['set' => $set],["projection"=>[
            '_id'=>1,
            'value'=>1,
            'set'=>1
        ]]);
        return $result;
    }
}