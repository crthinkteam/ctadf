<?php

namespace CTAF\DAO;

use MongoDB\BSON\ObjectId;

/**
 * UserDao
 */
class ProductDAO extends CollectionDAO
{
//    const USERNAME = 'userName';

    /**
     * construct method
     */
    public function __construct($mode = 'prod')
    {
        parent::__construct('products', $mode);
    }

    public function getProduct($mdid)
    {
        // TODO: use find one
        $result = parent::findByTypeMap(['_id' => new ObjectId($mdid)]);
        if ($result === null) {
            throw new \Exception("Product id '$mdid' not found.");
        }
        $result = $result[0];
        return $result;
    }

    public function getProductList()
    {
        $result = parent::findOption([],["projection"=>[
            '_id'=>1,
            'productName'=>1,
        ]]);
        return $result;
    }
}