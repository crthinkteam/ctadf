<?php

namespace CTAF\DAO;

use CTAF\Model\AreasBM;
use CTAF\Model\AreasBMArray;

class AreasDAO extends CollectionDAO
{
    //SchemaV2 enums
    const AREA_NAME = 'areaName';
    const PSR_NAME = 'psrName';

    public function __construct($mode = 'prod')
    {
        parent::__construct('areas', $mode);

    }

//    public function getAreas(string $code): AreasBM
//    {
//        $result = parent::findBy([AreasBM::PROFILE_TYPE => $code])[0];
//        if (empty($result)) {
//            return new AreasBM(true);
//        }
//        $profile_t = new AreasBM();
//        $profile_t->profile_type = $result[AreasBM::PROFILE_TYPE];
//        $profile_t->name = $result[AreasBM::NAME];
//        return $profile_t;
//    }

    public function getUserAreas($psrName) {
        $result = parent::findBy([AreasBM::PSRNAME => $psrName]);
        if (empty($result)) {
            return new AreasBM(true);
        }
        $retval = [];
        foreach ($result as $areaRes) {
            $area = [];
            $area['_id'] = $areaRes['_id'];
            $area['areaName'] = $areaRes[AreasBM::AREANAME];
            array_push($retval,$area);
        }
        return $retval;
    }

    public function allAreasList() {
        $result = parent::findBy([]);
        $retval = [];
        foreach ($result as $areaRes) {
            array_push($retval,$areaRes[AreasBM::AREANAME]);
        }
        return $retval;
    }

    public function createArea(AreasBM $area)
    {
//        TODO: validate area name is unique
        $data = [
            'areaName' => $area->areaName,
            'psrName' => $area->psrName
        ];
        $this->insertOne($data);
        return true;
    }

    public function updateArea(AreasBM $area): bool
    {
        if (property_exists($area,parent::_ID)) {
            $result = parent::getOne($area->{parent::_ID});
        } else {
            return false;
        }
        if (empty($result)) {
            return false;
        }
//        TODO: validate area name is unique
        $result[static::AREA_NAME] = $area->{static::AREA_NAME};
        $result[static::PSR_NAME] = $area->{static::PSR_NAME};
        $this->updateOne($result);
        return true;
    }

}
