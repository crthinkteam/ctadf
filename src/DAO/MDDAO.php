<?php

namespace CTAF\DAO;

use CTAF\Model\CHOrgBM;
use CTAF\Model\CHOrgBMArray;
use MongoDB\BSON\ObjectId;

/**
 * UserDao
 */
class MDDAO extends CollectionDAO
{
//    const USERNAME = 'userName';

    /**
     * construct method
     */
    public function __construct($mode = 'prod')
    {
        parent::__construct('mds', $mode);
    }

    public function createMD($mdInfo)
    {
        $data = [];
        foreach ($mdInfo['locations'] as $k => $v) {
            $data['locations'][$k]['location'] = $v['location'];
            $data['locations'][$k]['schedule'] = $v['schedule'];
        }
        $data['firstName'] = $mdInfo['firstName'];
        $data['lastName'] = $mdInfo['lastName'];
        foreach ($mdInfo['mdcontactInformation'] as $k => $v) {
            $data['mdcontactInformation'][$k]['infoType'] = $v['infoType'];
            $data['mdcontactInformation'][$k]['infoValue'] = $v['infoValue'];
        }
        foreach ($mdInfo['secretaryList'] as $k => $v) {
            $data['secretaryList'][$k]['firstName'] = $v['firstName'];
            $data['secretaryList'][$k]['lastName'] = $v['lastName'];
            foreach($v['contactInformation'] as $cik => $civ) {
                $data['secretaryList'][$k]['contactInformation'][$cik]['infoType'] =
                    $civ['infoType'];
                $data['secretaryList'][$k]['contactInformation'][$cik]['infoValue'] =
                    $civ['infoValue'];
            }
        }
        foreach ($mdInfo['products'] as $k => $v) {
            $data['products'][$k]['product'] = $v['product'];
            $data['products'][$k]['datestarted'] = $v['datestarted'];
            if (array_key_exists('datestopped',$v))
                $data['products'][$k]['datestopped'] = $v['datestopped'];
        }
        foreach ($mdInfo['specialty'] as $k => $v) {
            $data['specialty'][$k] = $v;
        }
        foreach ($mdInfo['subspecialty'] as $k => $v) {
            $data['subspecialty'][$k] = $v;
        }
        foreach ($mdInfo['potentials'] as $k => $v) {
            $data['potentials'][$k]['potential'] = $v['potential'];
            $data['potentials'][$k]['datestarted'] = $v['datestarted'];
        }

        $newid = $this->insertOne($data);
        return $newid;
    }

    public function getMD($mdid)
    {
        // TODO: use find one
        $result = parent::findByTypeMap(['_id' => new ObjectId($mdid)]);
        if ($result === null) {
            throw new \Exception("MD id '$mdid' not found.");
        }
        $result = $result[0];
        return $result;
    }

    public function getCHMDList($username ="")
    {
//        TODO: filter by list of MD->ch->area assigned to user
        if ($username <> "") {
            $cursor = parent::collection()->aggregate([
                ['$lookup' => [
                    'from' => 'chorg',
                    'localField' => "areaName",
                    'foreignField' => "_id",
                    'as' => 'area'
                ]],
                ['$match' => [
                    'area.psrName' => $username
                ]]
            ]);
            $result = [];
            foreach ($cursor as $the) {
                if (isset($the[static::_ID])) {
                    $the[static::_ID] = $the[static::_ID]->__toString();
                }
                array_push($result, (array)$the);
            }
        } else {
            $result = parent::findOption([],["projection"=>[
                'areaName'=>1,
                'chorgName'=>1,
            ]]);
        }
        if ($result === null) {
            throw new \Exception("MD list is empty.");
        }
        return $result;
    }

    public function updateMD($md): bool
    {
        if (property_exists($md,parent::_ID)) {
            $result = parent::getOne($md->{parent::_ID});
        } else {
            return false;
        }
        if (empty($result)) {
            return false;
        }
        $result['chorgName'] = $md->{CHOrgBM::NAME};
        $result['address'] = $md->{CHOrgBM::ADDRESS};
        $result['landline'] = $md->{CHOrgBM::LANDLINE};
//        TODO: add validation of areaName against current areas list
        $result['areaName'] = $md->{CHOrgBM::AREA_NAME};
        if (in_array($md->{CHOrgBM::ORG_TYPE},CHOrgBM::ORG_TYPE_LIST)) {
            $result['orgType'] = $md->{CHOrgBM::ORG_TYPE};
        }
        $this->updateOne($result);
        return true;
    }
}