<?php

namespace CTAF\DAO;

use MongoDB\BSON\ObjectId;

/**
 * UserDao
 */
class SubSpecialtiesDAO extends CollectionDAO
{
//    const USERNAME = 'userName';

    /**
     * construct method
     */
    public function __construct($mode = 'prod')
    {
        parent::__construct('specialties', $mode);
    }

    public function getSubSpecialty($mdid)
    {
        // TODO: use find one
        $result = parent::findByTypeMap(['_id' => new ObjectId($mdid)]);
        if ($result === null) {
            throw new \Exception("Specialty id '$mdid' not found.");
        }
        $result = $result[0];
        return $result;
    }

    public function getSubSpecialtiesList()
    {
        $result = parent::findOption([],["projection"=>[
            '_id'=>1,
            'subspecialty'=>1,
        ]]);
        return $result;
    }
}