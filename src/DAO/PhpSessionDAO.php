<?php

namespace CTAF\DAO;

use MongoDB\Collection;

class PhpSessionDAO extends CollectionDAO
{

    public function __construct($mode = 'prod')
    {
        parent::__construct('ctaf_session', $mode);

    }

    public function collection(): Collection
    {
        return parent::collection();
    }
}
