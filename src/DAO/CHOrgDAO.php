<?php

namespace CTAF\DAO;

use CTAF\Model\CHOrgBM;
use CTAF\Model\CHOrgBMArray;
use MongoDB\BSON\ObjectId;

/**
 * UserDao
 */
class CHOrgDAO extends CollectionDAO
{
//    const USERNAME = 'userName';

    /**
     * construct method
     */
    public function __construct($mode = 'prod')
    {
        parent::__construct('chorg', $mode);
    }

    public function createCHOrg(CHOrgBM $chorg)
    {
        $data = [
            'chorgName' => $chorg->chorgName,
            'address' => $chorg->address,
            'landline' => $chorg->landline,
            'areaName' => $chorg->areaName,
            'orgType' => $chorg->orgType,
        ];
        $newid = $this->insertOne($data);
//        return $this->getCHOrg($newid);
        return $newid;
    }

    public function getCHOrg($chorgid): CHOrgBM
    {
//        $result = parent::findByTypeMap([UserBM::USER_NAME => $username])[0];
//        if ($result === null) {
//            throw new \Exception("User '$username' not found.");
//        }
//        $user = new UserBM($result,false);

        $result = parent::findByTypeMap(['_id' => new ObjectId($chorgid)]);
        if ($result === null) {
            throw new \Exception("CHOrg id '$chorgid' not found.");
        }
        $result = $result[0];
        $user = new CHOrgBM($result,false);
        return $user;
    }

    public function getCHOrgList($username ="")
    {
        if ($username <> "") {
            $cursor = parent::collection()->aggregate([
                ['$lookup' => [
                    'from' => 'areas',
                    'localField' => "areaName",
                    'foreignField' => "areaName",
                    'as' => 'area'
                ]],
                ['$match' => [
                    'area.psrName' => $username
                ]]
            ]);
            $result = [];
            foreach ($cursor as $the) {
                if (isset($the[static::_ID])) {
                    $the[static::_ID] = $the[static::_ID]->__toString();
                }
                array_push($result, (array)$the);
            }
        } else {
            $result = parent::findOption([],["projection"=>[
                'areaName'=>1,
                'chorgName'=>1,
            ]]);
        }
        if ($result === null) {
            throw new \Exception("CHOrg list is empty.");
        }
        return $result;
    }

    public function updateCHOrg(CHOrgBM $chorg): bool
    {
        if (property_exists($chorg,parent::_ID)) {
            $result = parent::getOne($chorg->{parent::_ID});
        } else {
            return false;
        }
        if (empty($result)) {
            return false;
        }
        $result['chorgName'] = $chorg->{CHOrgBM::NAME};
        $result['address'] = $chorg->{CHOrgBM::ADDRESS};
        $result['landline'] = $chorg->{CHOrgBM::LANDLINE};
//        TODO: add validation of areaName against current areas list
        $result['areaName'] = $chorg->{CHOrgBM::AREA_NAME};
        if (in_array($chorg->{CHOrgBM::ORG_TYPE},CHOrgBM::ORG_TYPE_LIST)) {
            $result['orgType'] = $chorg->{CHOrgBM::ORG_TYPE};
        }
        $this->updateOne($result);
        return true;
    }
}