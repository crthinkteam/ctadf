<?php

namespace CTAF\DAO;

use MongoDB\BSON\ObjectId;

/**
 * UserDao
 */
class ContactInfoTypeDAO extends CollectionDAO
{
//    const USERNAME = 'userName';

    /**
     * construct method
     */
    public function __construct($mode = 'prod')
    {
        parent::__construct('contactinfotype', $mode);
    }

    public function getMD($mdid)
    {
        // TODO: use find one
        $result = parent::findByTypeMap(['_id' => new ObjectId($mdid)]);
        if ($result === null) {
            throw new \Exception("MD id '$mdid' not found.");
        }
        $result = $result[0];
        return $result;
    }

    public function getContactInfoTypeList()
    {
        $result = parent::findOption([],["projection"=>[
            '_id'=>1,
            'infotype'=>1,
        ]]);
        return $result;
    }
}