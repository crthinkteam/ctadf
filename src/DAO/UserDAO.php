<?php

namespace CTAF\DAO;

use CTAF\Model\UserBM;

/**
 * UserDao
 */
class UserDAO extends CollectionDAO
{
    const USERNAME = 'userName';
    const PASSWORD = 'password';
    const FIRST_NAME = 'givenName';
    const LAST_NAME = 'familyName';
    const MIDDLE_NAME = 'middleName';
    const TITLE = 'title';
    const ROLE = 'roles';

    /**
     * construct method
     */
    public function __construct($mode = 'prod')
    {
        parent::__construct('users', $mode);
    }

    public function createBySso($username, $firstname, $lastname, $email): UserBM
    {
        $data = [
            'userName' => $username,
            'name' => [self::FIRST_NAME => $firstname, self::LAST_NAME =>  $lastname],
            'roles' => [],
            'email' => $email,
            'active'=>true,
            'title'=>'',
            'company' => 'CTAF'
        ];
        $this->insertOne($data);
        return $this->getUser($username);
    }

    public function createUser(UserBM $user)
    {
        $data = [
            'userName' => $user->username,
            'name' => [self::FIRST_NAME => $user->givenName, self::MIDDLE_NAME =>  $user->middleName, self::LAST_NAME =>  $user->familyName],
            'roles' => $user->roles,
            'email' => $user->email,
            'title' => $user->title,
            'password' => $user->password,
            'active' => $user->active,
            'company' => 'CTAF'
        ];
        $this->insertOne($data);
        return $this->getUser($user->username);
    }

    public function getUser(string $username): UserBM
    {
        $result = parent::findByTypeMap([UserBM::USER_NAME => $username])[0];
        if ($result === null) {
            throw new \Exception("User '$username' not found.");
        }
        $user = new UserBM($result,false);
        return $user;
    }

    public function login(string $username): UserBM
    {
        $result = parent::findByTypeMap([UserBM::USER_NAME => $username, UserBM::ACTIVE => true])[0];
        if ($result === null) {
            throw new \Exception("User '$username' not found.");
        }
        $user = new UserBM($result, false);
        $user->password = $result['password'];
        return $user;
    }

    public function updateUser(UserBM $user): bool
    {
        $result = parent::findBy([static::USERNAME => $user->username]);
        if (empty($result)) {
            return false;
        }
        $result = $result[0];  // @TODO: result is an array of results this should be a findOne
        $result['name'] = [self::FIRST_NAME => $user->givenName, self::MIDDLE_NAME =>  $user->middleName,self::LAST_NAME =>  $user->familyName];
        $result['roles'] = $user->roles;
        $result['active'] = $user->active;
        $result['title'] = $user->title;
        $result['email'] = $user->email;
        $result['active'] = $user->active;
        if(!empty($user->password)){
            $result['password'] = $user->password;
        }
        $this->updateOne($result);
        return true;
    }
}