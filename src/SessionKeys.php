<?php

use CTAF\Model\UserBM;

/**
 * Centralized container for session keys
 */
class SessionKeys {
    /**
     *  must contain a UserBM value
     */
    const USER_BM = 'UserBM';
    const PROFILE_TYPE = 'profile_type';

//    const SAML2_IDP = 'saml2.idp';
//    const SAML2_ATTR = 'saml2.attributes';
//    const SAML2_NAMEID = 'saml2.nameid';
//    const SAML2_NAMEID_FORMAT = 'saml2.nameidformat';
//    const SAML2_USERNAME = 'saml2.USERNAME';

    const LOGIN_ROUTE = 'login_url';
}
