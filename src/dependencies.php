<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

//// mustache
//$container['view'] = function ($c) {
//    $mustache_options = array('extension' => 'mustache.html');
//	$v = new Slim\Views\Mustache([
//		'loader' => new Mustache_Loader_FilesystemLoader( __DIR__ . '/../tpl', $mustache_options)
//	]);
//	return $v;
//};
//
//// still mustache with page wrapper
//$container['viewwrapped'] = function ($c) {
//    $mustache_options = array('extension' => 'mustache.html');
//	$v = new BFD\Middleware\WrappedMustache([
//		'loader' => new Mustache_Loader_FilesystemLoader( __DIR__ . '/../tpl', $mustache_options)
//	]);
//	return $v;
//};

$container['flash'] = function ($c) {
    return new \Slim\Flash\Messages();
};

$container['session'] = function ($c) {
    return new \SlimSession\Helper;
};