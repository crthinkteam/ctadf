<?php

namespace CTAF\Model;

use CTAF\Model\BusinessModel;

class LookupBM extends BusinessModel
{
    const CODE = 'code';
    const NAME = 'name';
    const DESCRIPTION = 'description';
    /**
     * @var string
     */
    public $collection;

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $name;
    public $description;


    public function __construct(bool $is_null = false)
    {
        parent::__construct($is_null);
    }

    /**
     * Returns an array of strings which are keys for the database IDs of the implementing class.
     *
     * @return array
     */
    public function ids(): array
    {
        return [];
    }
}