<?php

namespace CTAF\Model;


abstract class BusinessModelArray
{
    private $data = array();

    public function add($element)
    {
        $this->data[] = $element;
    }

    public function get(int $position)
    {
        return $this->data[$position];
    }

    public function size(): int
    {
        return sizeof($this->data);
    }
}