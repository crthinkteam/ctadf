<?php

namespace CTAF\Model;

class CHOrgBM extends BusinessModel
{
    const NAME = 'chorgName';
    const ADDRESS = 'address';
    const LANDLINE = 'landline';
    const AREA_NAME = 'areaName';
    const ORG_TYPE = 'orgType';
    const ORG_TYPE_LIST = [
        'Clinic',
        'Hospital'
    ];

    public $_id;
    public $chorgName;
    public $address;
    public $landline;
    public $areaName;
    public $orgType;

    public function __construct($chorg, bool $is_null = false)
    {
        parent::__construct($is_null);
        $this->chorgName = "";
        $this->address = "";
        $this->landline = "";
        $this->areaName = "";
        $this->orgType = "";
        if(!empty($chorg)){
            $a = [
                '_id',
                'chorgName',
                'address',
                'landline',
                'areaName',
                'orgType'
            ];
            foreach ($a as $prop) {
                if (array_key_exists($prop, $chorg) && !empty($chorg[$prop])) {
                    $this->{$prop} = $chorg[$prop];
                }
            }
        }
    }
}