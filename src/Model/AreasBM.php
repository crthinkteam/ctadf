<?php

namespace CTAF\Model;

class AreasBM extends BusinessModel
{
    const AREANAME = 'areaName';
    const PSRNAME = 'psrName';
    public $areaName;
    public $psrName;
    public $_id;

    public function __construct(bool $is_null = false)
    {
        parent::__construct($is_null);
        $this->areaName = "";
    }

    /**
     * Returns an array of strings which are keys for the database IDs of the implementing class.
     *
     * @return array
     */
    public function ids(): array
    {
        return [];
    }
}