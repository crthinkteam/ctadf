<?php

namespace BFD\Model;

class ProfileTypeBMArray extends BusinessModelArray
{
    public function add(ProfileTypeBM $element)
    {
        parent::add($element);
    }

    public function get(int $position): ProfileTypeBM
    {
        return parent::get($position);
    }
}