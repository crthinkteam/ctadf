<?php

namespace BFD\Model;

class ProfileTypeBM extends BusinessModel
{
    const PROFILE_TYPE = 'profile_type';
    const NAME = 'name';
    /**
     * @var string
     */
    public $profile_type;

    /**
     * @var string
     */
    public $name;

    public function __construct(bool $is_null = false)
    {
        parent::__construct($is_null);
    }

    /**
     * Returns an array of strings which are keys for the database IDs of the implementing class.
     *
     * @return array
     */
    public function ids(): array
    {
        return [];
    }
}