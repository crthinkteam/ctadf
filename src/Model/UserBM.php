<?php

namespace CTAF\Model;

class UserBM extends BusinessModel
{
    const USER_NAME = 'userName';
    const GIVEN_NAME = 'givenName';
    const MIDDLE_NAME = 'middleName';
    const FAMILY_NAME = 'familyName';
    const EMAIL = 'email';
    const ROLES = 'roles';
    const TITLE = 'title';
    const PASSWORD = 'password';
    const ACTIVE = 'active';


    const ROLESET = [
        'admin' => 'Admin',
        'psr' => 'Professional Sales Representative'
    ];

    public $username;
    public $email;
    public $familyName;
    public $givenName;
    public $middleName;
    public $roles;
    public $title;
    public $password;

    public function __construct($user, bool $is_null = false)
    {
        parent::__construct($is_null);
        $this->username = "";
        $this->email = "";
        $this->active = false;
        $this->familyName = "";
        $this->givenName = "";
        $this->middleName = "";
        $this->roles = [];
        $this->title = "";
        $this->password = "";
        if(!empty($user)){
            if(!empty($user[self::TITLE])) {
                $this->title = $user[self::TITLE];
            }
            if(!empty($user[self::USER_NAME])) {
                $this->username = $user[self::USER_NAME];
            }
            if(!empty($user[self::ACTIVE])) {
                $this->active = $user[self::ACTIVE];
            }
            if(!empty($user[self::EMAIL])) {
                $this->email = $user[self::EMAIL];
            }
            if(!empty($user['name'][self::FAMILY_NAME])) {
                $this->familyName = $user['name'][self::FAMILY_NAME];
            }
            if(!empty($user['name'][self::GIVEN_NAME])) {
                $this->givenName = $user['name'][self::GIVEN_NAME];
            }
            if(!empty($user['name'][self::MIDDLE_NAME])) {
                $this->middleName = $user['name'][self::MIDDLE_NAME];
            }
            if(!empty($user[self::ROLES])) {
                if(is_array( $user[self::ROLES])) {
                    $this->roles = $user[self::ROLES];
                } else {
                    $this->roles = [$user[self::ROLES]];
                }
            }
        }
    }

    public function inRole(string $role)
    {
        return in_array($role, $this->roles);
    }

    public function isActive()
    {
        return  $this->active;
    }

    public function userName(): string
    {
        return $this->username;
    }

    public function displayName(): string
    {
        return trim("{$this->givenName} "
            . "{$this->middleName} {$this->familyName}");
//        return "{$this->attributes[static::GIVEN_NAME]} "
//            . "{$this->attributes[static::MIDDLE_NAME]} {$this->attributes[static::FAMILY_NAME]}";
    }
}