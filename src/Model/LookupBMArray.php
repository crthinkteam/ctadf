<?php

namespace CTAF\Model;


class LookupBMArray extends BusinessModelArray
{
    public function add(LookupBM $element)
    {
        parent::add($element);
    }

    public function get(int $position): LookupBM
    {
        return parent::get($position);
    }

}

