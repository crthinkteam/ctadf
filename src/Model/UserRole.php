<?php

namespace CTAF\Model;

interface UserRole
{
    const PSR = 'psr';
    const ADMIN = 'admin';
    const NONE = 'none';
}