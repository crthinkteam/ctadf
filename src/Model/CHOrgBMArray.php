<?php

namespace CTAF\Model;

class CHOrgBMArray extends BusinessModelArray
{
    public function add(CHOrgBM $element)
    {
        parent::add($element);
    }

    public function get(int $position): CHOrgBM
    {
        return parent::get($position);
    }
}