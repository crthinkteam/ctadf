<?php

namespace CTAF\Model;

abstract class BusinessModel
{
    /**
     *
     * @var array
     */
    protected $attributes = [];
    public $data = [];

    private $isNull = true;

    public function __construct(bool $is_null)
    {
        $this->isNull = $is_null;
    }

    public function isNull() : bool
    {
        return $this->isNull;
    }

    public function att(string $att)
    {
        return $this->attributes[$att];
    }

    /**
     * Should be overridden by the implementing class and signature MUST return an instance of
     * the implementing class.
     *
     * Implementation MUST return a new instance, not a reference to the current instance.
     *
     * @param array $newAttributes
     * @return mixed
     * @throws \Exception
     */
    protected function set(array $newAttributes)
    {
        $new_attributes = $this->attributes;
        $constants = (new \ReflectionClass(static::class))->getConstants();
        foreach ($constants as $k => $v) {
            if (in_array($v, $this->ids(), true)) {
                throw new \Exception("Do not reset ID: {$v}.");
            }
            $new_attributes[$v] = $newAttributes[$v];
        }
        return new $this->__construct($new_attributes);
    }


    /**
     * Returns an array of strings which are keys for the database IDs of the implementing class.
     *
     * @return array
     */
    public function ids(): array
    {
        // TODO: Implement ids() method.
        return [];
    }

}
