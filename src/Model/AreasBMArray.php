<?php

namespace CTAF\Model;

class AreasBMArray extends BusinessModelArray
{
    public function add(AreasBM $element)
    {
        parent::add($element);
    }

    public function get(int $position): AreasBM
    {
        return parent::get($position);
    }
}