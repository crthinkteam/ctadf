<?php

include_once 'RouteRegistry.php';

use Slim\Http\Request;
use Slim\Http\Response;
use CTAF\DAO\AreasDAO;

$app->get(RouteRegistry::ROOT, function (Request $request, Response $response, array $args) {
    if ($this->session->exists(SessionKeys::USER_BM)) {
        return $response->withRedirect(RouteRegistry::HOME);
    } else {
        return $response->withRedirect(RouteRegistry::LOGIN);
    }
})->setName(RouteRegistry::ROOT);

$app->get(RouteRegistry::LOGIN, function (Request $request, Response $response, array $args) {
    // Already loggedin, redirect to home
    if ($this->session->exists(SessionKeys::USER_BM)) {
        return $response->withRedirect(RouteRegistry::HOME);
    }

    return $response->withRedirect(RouteRegistry::LOGIN_EXTERNAL);

})->setName(RouteRegistry::LOGIN);

$app->get(RouteRegistry::LOGOUT, function (Request $request, Response $response, array $args) {
    $login_route = $this->session->get(SessionKeys::LOGIN_ROUTE);
    if ($login_route === RouteRegistry::LOGIN_EXTERNAL) {
        $this->session::destroy();
        return $response->withRedirect(RouteRegistry::LOGIN_EXTERNAL);
    }
    return $response->withRedirect('/sso/logout');
})->setName(RouteRegistry::LOGOUT);

// require modules as needed
require __DIR__ . '/Controllers/LoginExternalCtl.php';
require __DIR__ . '/Controllers/HomeCtl.php';
require __DIR__ . '/Controllers/UserCtl.php';
require __DIR__ . '/Controllers/AreasCtl.php';
require __DIR__ . '/Controllers/AreasDataCtl.php';
require __DIR__ . '/Controllers/CHOrgCtl.php';
require __DIR__ . '/Controllers/CHOrgDataCtl.php';
require __DIR__ . '/Controllers/MDCtl.php';
require __DIR__ . '/Controllers/MDDataCtl.php';

//require __DIR__ . '/Controllers/CountryCtl.php';
//require __DIR__ . '/Controllers/ScopeCtl.php';
//require __DIR__ . '/Controllers/RelationshipTypeCtl.php';
//require __DIR__ . '/Controllers/ProfileCtl.php';
//require __DIR__ . '/Controllers/ReportsCtl.php';
//require __DIR__ . '/Controllers/ReportimporterCtl.php';
//require __DIR__ . '/Controllers/BuilderCtl.php';
require __DIR__ . '/Controllers/TplCtl.php';
require __DIR__ . '/Controllers/CmsManageCtl.php';
//require __DIR__ . '/Controllers/ProfileTypeCtl.php';
//require __DIR__ . '/Controllers/IconCtl.php';
//require __DIR__ . '/Controllers/AltcountriesCtl.php';
//require __DIR__ . '/Controllers/AltindustriesCtl.php';
//require __DIR__ . '/Controllers/DBStatsCtl.php';
//require __DIR__ . '/Controllers/Saml2Ctl.php';
//require __DIR__ . '/Controllers/SsoCtl.php';
//require __DIR__ . '/Controllers/Error404Ctl.php';
//require __DIR__ . '/Controllers/TestCtl.php';
