<?php

use Slim\Http\Request;
use Slim\Http\Response;
use MongoDB\BSON\Regex;

use CTAF\Controllers\MDEditPage;
use CTAF\Controllers\MDListPage;
use CTAF\DAO\MDDAO;
use CTAF\DAO\CHORgDAO;
use CTAF\Model\CHOrgBM;
use CTAF\Model\UserRole;

// open page of list of MDs
$app->get(RouteRegistry::MDS_LIST, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    $flash = $this->flash->getFirstMessage(RouteRegistry::MDS_LIST);
    $page = new MDListPage($user, $flash);
    return $response->write($page->render());
})->setName(RouteRegistry::MDS_LIST);

// open page of form for new MD
$app->get(RouteRegistry::MDS_NEW, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    $flash = $this->flash->getFirstMessage(RouteRegistry::MDS_NEW);
    $page = new MDEditPage($user, "POST", json_encode([]), $flash);
    return $response->write($page->render());
})->setName(RouteRegistry::MDS_NEW);

// redirect to md list
$app->get(RouteRegistry::MDS, function (Request $request, Response $response, array $args) {
    return $response->withRedirect(RouteRegistry::MDS_LIST);
})->setName(RouteRegistry::MDS);
$app->get(RouteRegistry::MDSS, function (Request $request, Response $response, array $args) {
    return $response->withRedirect(RouteRegistry::MDS_LIST);
})->setName(RouteRegistry::MDSS);

// open page of form for editing existing md
$app->get(RouteRegistry::MDS_EDIT, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);

    if (array_key_exists('_id',$args) && ($args['_id'] <> "")) {
        try {
            $md_edit = (new MDDAO())->getMD($args['_id']);
        } catch (Exception $e) {
//            $this->flash->addMessage(RouteRegistry::CHORG_LIST, $e->getMessage());
            $this->flash->addMessage(RouteRegistry::MDS_LIST, "Record not found");
            return $response->withRedirect(RouteRegistry::MDS_LIST);
        }
    } else {
        $this->flash->addMessage(RouteRegistry::MDS_LIST, "No ID provided.");
        return $response->withRedirect(RouteRegistry::MDS_LIST);
    }
    $flash = $this->flash->getFirstMessage(RouteRegistry::MDS_EDIT);

    $page = new MDEditPage($user, "PUT", json_encode($md_edit) , $flash);
    return $response->write($page->render());
})->setName(RouteRegistry::MDS_EDIT);

//$app->post(RouteRegistry::CHORG_EDIT, function (Request $request, Response $response, array $args) {
//    $all_vars = $request->getParsedBody();
//    $user = $this->session->get(SessionKeys::USER_BM);
//    return $response->write("wasaaapenin" . $all_vars);
//})->setName(RouteRegistry::CHORG_EDIT);
