<?php

namespace CTAF\Controllers\Template;

use CTAF\Model\LookupBMArray;
use CTAF\Model\ProfileSectionBM;
use CTAF\Model\ProfileTypeBMArray;
use CTAF\Model\UserBM;
use StampTemplateEngine\StampTE;
use CTAF\DAO\ProfileSearchResults;

class CmsQaid extends BaseQaid
{
    const TEMPLATE_FILE = StampTemplate::TEMPLATE_DIR . 'cms-qaid.html';

    const V_TITLE_BODY = 'V_TITLE_BODY';

    const JS_DATA_TABLES = 'js_data_tables';
    const JS_I_CHECK = 'js_i_check';
    const JS_JQUERY_VALIDATE = 'js_jquery_validate';
    const JS_SUMMER_NOTE = 'js_summer_note';
    const JS_RCC = 'js_rcc';
    const JS_EDITOR = 'js_editor';
    const JS_PROFILE = 'js_profile';
    const JS_SELECT2 = 'js_select2';
    const CSS_SELECT2 = 'css_select2';
    const CSS_SMART_WIZARD_THEME_CIRCLE = 'css_smart_wizard_theme_circle';
    const CSS_I_CHECK = 'css_i_check';
    const CSS_SUMMER_NOTE = 'css_summer_note';
    const CSS_DATA_TABLES = 'css_data_tables';
    const CSS_SURVEY_EDITOR = 'css_survey_editor';

    public static function slugify(string $to_slug)
    {
        $trimmed = trim($to_slug);
        $despaced = str_replace(' ', '', $trimmed);
        return urlencode($despaced);
    }

    public function __construct(UserBM $user)
    {
        $this->stamp = new StampTE(file_get_contents(static::TEMPLATE_FILE));
        global $appname;
        $this->stamp->inject('app_title', $appname);
        parent::__construct($user);
    }

    public function setContent($content)
    {
        $this->stamp->inject('CONTENT', $content, true);
    }

    /**
     * $slot would be one of the CSS_* or JS_* static constants.
     *
     * @param string $slot
     */
    public function addCutBlock(string $slot)
    {
        $snippet = $this->stamp->get($slot);
        $this->stamp->add($snippet);
    }


    public function setTitle(string $title)
    {
        $this->stamp->inject('TITLE_HEAD', $title);
        $this->stamp->inject('TITLE_PAGE', $title);
    }

    public function setTitleHead(string $title)
    {
        $this->stamp->inject('TITLE_HEAD', $title);
    }

    public function setTitlePage(string $title)
    {
        $this->stamp->inject('TITLE_PAGE', $title);
    }


    public function setTitleBody(string $title)
    {
//        $block = $this->stamp->get('title_body');
//        $block->inject('title', $title);
//        $this->stamp->add($block);

        $this->stamp->add(($this->stamp->get('title_body'))->inject('title', $title));

    }

    public function setVersion(string $version)
    {
        $block = $this->stamp->get('footer');
        $block->inject('version', $version);
        $this->stamp->add($block);
    }



    public function addBreadCrumb(string $label)
    {
        $crumb = $this->stamp->get('breadcrumb')->copy();
        $crumb->inject('V_crumb_label', $label);
        $this->stamp->glue('breadcrumb_paste', $crumb);
    }

    private $page_wide = false;

    public function toggleMakePageWide(bool $tf = null)
    {
        $this->page_wide = $tf;
    }

    public function toggleAdminLink()
    {
        $nav = $this->stamp->get('nav_manage');
        $nav->inject('link_cms_manage', \RouteRegistry::CMS_MANAGE);
        $this->stamp->add($nav);
    }

    public function showCmsManage()
    {
        $cmscontent = $this->stamp->get('content');
        $manage = $cmscontent->get('cms_manage');
        $manage->inject('link_areas_list', \RouteRegistry::AREAS);
        $manage->inject('link_areas_orgs', \RouteRegistry::CHORG_LIST);
        $manage->inject('link_md_list', \RouteRegistry::MDS_LIST);
        $manage->inject('link_terprog', \RouteRegistry::TERRITORY);
        $manage->inject('link_mdmasterlist', \RouteRegistry::MDMASTERLIST);
        $cmscontent->add($manage);
        $this->stamp->add($cmscontent);
    }

    public function showCmsAdmin()
    {
        $cmscontent = $this->stamp->get('content');
        $manage = $cmscontent->get('cms_manage_admin');
        $manage->inject('link_areas_list', \RouteRegistry::AREAS);
        $manage->inject('link_all_users', \RouteRegistry::USERS);
        $cmscontent->add($manage);
        $this->stamp->add($cmscontent);
    }

    public function showFlash(string $msg)
    {
        $flash = $this->stamp->get('flash');
        $flash->inject('msg', $msg);
        $this->stamp->add($flash);
    }

    public function render(): string
    {
        if ($this->page_wide) {
            $style = $this->stamp->get('page_wide');
            $this->stamp->add($style);
        }
        return $this->stamp;
    }

    public function testRenderNotWide()
    {
        $this->testRender();
        $this->toggleMakePageWide(false);
        return $this->render();
    }

    public function testRenderWide()
    {
        $this->testRender();
        $this->toggleMakePageWide(true);
        return $this->render();
    }

    public function addCustomScriptBlock($js)
    {
        $this->stamp->glue('custom_scriptblock', $js);
    }

    public function add(StampTE $stampobj) {
        $this->stamp->add($stampobj);
    }

}