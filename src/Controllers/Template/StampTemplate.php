<?php

namespace CTAF\Controllers\Template;

/**
 * Interface marker for all classes using StampTE
 */
interface StampTemplate
{
    const TEMPLATE_DIR = __DIR__ . '/../../../public/templates/';

    public function render() : string;
}