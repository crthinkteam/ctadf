<?php

namespace CTAF\Controllers\Template;

use CTAF\Model\UserRole;
use StampTemplateEngine\StampTE;
use CTAF\Model\UserBM;

class BaseQaid
{

    protected $stamp;

    public function __construct(UserBM $user)
    {
        global $ctafversion;
        $this->stamp->inject('SHARP', '#');
        $this->setUser($user);
        $this->setVersion($ctafversion);
    }

    protected function setUser(UserBM $user)
    {
        $this->stamp->inject('USERNAME', $user->userName());
        $this->stamp->inject('USER_FULL_NAME', $user->displayName());
        $this->stamp->inject('USER_TITLE', $user->title);
        if (!$user->inRole(UserRole::PSR)  && !$user->inRole(UserRole::ADMIN)) {
            $this->stamp->inject('role_is_readonly', 'true');
        }
    }

    protected function setVersion(string $version)
    {
        $block = $this->stamp->get('footer');
        $block->inject('version', $version);
        $this->stamp->add($block);
    }
}