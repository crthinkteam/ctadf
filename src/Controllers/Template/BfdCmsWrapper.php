<?php
/**
 * Created by PhpStorm.
 * User: eyan
 * Date: 09/09/2018
 * Time: 6:33 PM
 */

namespace CTAF\Controllers\Template;

use CTAF\Model\UserBM;
use CTAF\Model\UserRole;
use StampTemplateEngine\StampTE;

class BfdCmsWrapper implements StampTemplate
{
    const TEMPLATE_FILE = StampTemplate::TEMPLATE_DIR . 'bfd-wrapper.html';
    const JS_I_CHECK = 'js_i_check';
    const JS_RED_CONNECT = 'js_red_connect';
    const CSS_I_CHECK = 'css_i_check';

    private $stamp;
    private $user;
    private $titleHead;
    private $titlePage;
    private $flash;
    private $content;
    private $js_blocks;

    public function __construct(
        UserBM $user,
        string $titleHead,
        string $titlePage,
        string $content,
        array $jsBlocks,
        string $flash = null)
    {
        $this->user = $user;
        $this->titleHead = $titleHead;
        $this->titlePage = $titlePage;
        $this->flash = $flash;
        $this->content = $content;
        $this->js_blocks = $jsBlocks;

        $this->fill();
    }

    private function fill()
    {
        $stamp = new StampTE(file_get_contents(static::TEMPLATE_FILE));
        $stamp->inject('TITLE_HEAD', $this->titleHead);
        $stamp->inject('TITLE_PAGE', $this->titlePage);
        $stamp->inject('V_LINK_HOME', \RouteRegistry::HOME);
        $stamp->inject('V_LINK_LOGIN_EXTERNAL', \RouteRegistry::LOGIN_EXTERNAL);
        $stamp->inject('V_LINK_LOGIN_EXTERNAL', \RouteRegistry::LOGIN_EXTERNAL);

        $user = $this->user;
        $full_name = "$user->givenName $user->middleName $user->familyName";
        $stamp->inject('USERNAME', $user->username);
//        $stamp->inject('USERNAME', $user->givenName);
        $stamp->inject('USER_FULL_NAME', $full_name);
        $stamp->inject('USER_TITLE', $user->title);

        if ($this->flash !== null) {
            $flash = $stamp->get('flash');
            $flash->inject('msg', $this->flash);
            $stamp->add($flash);
        }
        $stamp->inject('CONTENT', $this->content, true);

        if ($this->user->inRole(UserRole::ADMIN)) {
            $manage = $stamp->get('nav_manage');
            $manage->inject('link_cms_manage', \RouteRegistry::CMS_MANAGE);
            $stamp->add($manage);
        }

        foreach ($this->js_blocks as $j) {
            $block = $stamp->get($j);
            $stamp->add($block);
        }

        $this->stamp = $stamp;
    }

    public function render(): string
    {
        return $this->stamp;
    }

    public function addCustomScriptBlock($js) {
        $this->stamp->glue('custom_scriptblock', $js, false);
    }
    public function get($idstr) {
        return $this->stamp->get($idstr);
    }
    public function add(StampTE $stampobj) {
        $this->stamp->add($stampobj);
    }
}