<?php

namespace CTAF\Controllers\Template;

use StampTemplateEngine\StampTE;

class LoginTemplate implements StampTemplate
{
    const TEMPLATE_FILE = StampTemplate::TEMPLATE_DIR . 'login.html';

    private $stamp;

    public function __construct()
    {
        $this->stamp = new StampTE(file_get_contents(static::TEMPLATE_FILE));
        $this->stamp->inject('link_login_action', \RouteRegistry::LOGIN_EXTERNAL_P);
        global $appname;
        $this->stamp->inject('app_title', $appname);
    }

    public function showFlash($msg) {
        if (!$msg) {
            return;
        }
        $tmsg = trim($msg);
        if (!$tmsg) {
            return;
        }
        $flash = $this->stamp->get('flash');
        $flash->inject('flash_message', $tmsg);
        $this->stamp->add($flash);
    }

    public function render() :string {
        return $this->stamp;
    }
}