<?php

namespace CTAF\Controllers\Template;

use CTAF\Model\LookupBMArray;
use CTAF\Model\ProfileSectionBM;
use CTAF\Model\ProfileTypeBMArray;
use CTAF\Model\UserBM;
use StampTemplateEngine\StampTE;


class ErrorQaid extends BaseQaid
{
    const TEMPLATE_FILE = StampTemplate::TEMPLATE_DIR . 'cms-error.html';

    const V_TITLE_BODY = 'V_TITLE_BODY';

    const JS_DATA_TABLES = 'js_data_tables';
    const JS_I_CHECK = 'js_i_check';
    const JS_JQUERY_VALIDATE = 'js_jquery_validate';
    const JS_SUMMER_NOTE = 'js_summer_note';
    const JS_RED_CONNECT = 'js_red_connect';
    const JS_EDITOR = 'js_editor';
    const JS_PROFILE = 'js_profile';
    const JS_SELECT2 = 'js_select2';
    const CSS_SELECT2 = 'css_select2';
    const CSS_SMART_WIZARD_THEME_CIRCLE = 'css_smart_wizard_theme_circle';
    const CSS_I_CHECK = 'css_i_check';
    const CSS_SUMMER_NOTE = 'css_summer_note';
    const CSS_DATA_TABLES = 'css_data_tables';
    const CSS_SURVEY_EDITOR = 'css_survey_editor';

    private $errorcode;

    public static function slugify(string $to_slug)
    {
        $trimmed = trim($to_slug);
        $despaced = str_replace(' ', '', $trimmed);
        return urlencode($despaced);
    }

    public function __construct(UserBM $user, $errorcode = 404)
    {
        $this->stamp = new StampTE(file_get_contents(static::TEMPLATE_FILE));
        parent::__construct($user);
        $this->errorcode = $errorcode;
        switch ($errorcode) {

            case 403:
                $this->setTitle("Ooops - Something went wrong!");
                $this->addError("You are not allowed to view this page, please contact your system administrator or manager if you think this is a mistake?");
                break;

            case 404:
                $this->setTitle("Ooops - Something went wrong!");
                $this->addError("We cant find the page you are looking for, can we interest you in some others?");
                break;

            case 500:
                $this->setTitle("Ooops - Something went wrong!");
                $this->addError("A system error has occured, please record the steps to reproduce this issue and report it to your system administrator or manager, who will forward it to the development team for correction.");
                break;
        }
    }

    /**
     * $slot would be one of the CSS_* or JS_* static constants.
     *
     * @param string $slot
     */
    public function addCutBlock(string $slot)
    {
        $snippet = $this->stamp->get($slot);
        $this->stamp->add($snippet);
    }

    public function addError($errortext)
    {
        $error_blk = $this->stamp->get('error_stats');
        $error_blk->inject("error_string", $errortext);
        $this->stamp->add($error_blk);
    }


    public function setTitle(string $title)
    {
        $this->stamp->inject('TITLE_HEAD', $title);
        $this->stamp->inject('TITLE_PAGE', $title);
    }

    public function setTitleHead(string $title)
    {
        $this->stamp->inject('TITLE_HEAD', $title);
    }

    public function setTitlePage(string $title)
    {
        $this->stamp->inject('TITLE_PAGE', $title);
    }


    public function setTitleBody(string $title)
    {
        $block = $this->stamp->get('title_body');
        $block->inject('title', $title);
        $this->stamp->add($block);
    }

    public function addBreadCrumb(string $label)
    {
        $crumb = $this->stamp->get('breadcrumb')->copy();
        $crumb->inject('V_crumb_label', $label);
        $this->stamp->glue('breadcrumb_paste', $crumb);
    }

    public function toggleShowCreateProfileButton()
    {
        $btn = $this->stamp->get('header_profile_create_button');
        $this->stamp->add($btn);
    }

    private $page_wide = false;

    public function toggleMakePageWide(bool $tf = true)
    {
        $this->page_wide = $tf;
    }

    public function toggleAdminLink()
    {
        $nav = $this->stamp->get('nav_manage');
        $nav->inject('link_cms_manage', \RouteRegistry::CMS_MANAGE);
        $this->stamp->add($nav);
    }


    public function render(): string
    {
        if ($this->page_wide) {
            $style = $this->stamp->get('page_wide');
            $this->stamp->add($style);
        }
        return $this->stamp;
    }


    public function addCustomScript($js)
    {
        $this->stamp->glue('custom_scripts', $js, false);
    }


}