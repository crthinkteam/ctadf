<?php

use Slim\Http\Request;
use Slim\Http\Response;
use MongoDB\BSON\Regex;

use CTAF\Controllers\CHOrgEditPage;
use CTAF\Controllers\CHOrgListPage;
use CTAF\DAO\CHORgDAO;
use CTAF\Model\CHOrgBM;
use CTAF\Model\UserRole;

// open page of list of clinics/hospitals
$app->get(RouteRegistry::CHORG_LIST, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    $flash = $this->flash->getFirstMessage(RouteRegistry::CHORG_LIST);
    $page = new CHOrgListPage($user, $flash);
    return $response->write($page->render());
})->setName(RouteRegistry::CHORG_LIST);

// open page of form for new clinic/hospital
$app->get(RouteRegistry::CHORG_NEW, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    $flash = $this->flash->getFirstMessage(RouteRegistry::CHORG_NEW);
    $page = new CHOrgEditPage($user, "POST", json_encode([]), $flash);
    return $response->write($page->render());
})->setName(RouteRegistry::CHORG_NEW);

// redirect to chog list
$app->get(RouteRegistry::CHORG, function (Request $request, Response $response, array $args) {
    return $response->withRedirect(RouteRegistry::CHORG_LIST);
})->setName(RouteRegistry::CHORG);
$app->get(RouteRegistry::CHORG_S, function (Request $request, Response $response, array $args) {
    return $response->withRedirect(RouteRegistry::CHORG_LIST);
})->setName(RouteRegistry::CHORG_S);

// open page of form for editing existing clinic/hospital
$app->get(RouteRegistry::CHORG_EDIT, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);

    if (array_key_exists('_id',$args) && ($args['_id'] <> "")) {
        try {
            $chorg_edit = (new CHOrgDAO())->getCHOrg($args['_id']);
        } catch (Exception $e) {
//            $this->flash->addMessage(RouteRegistry::CHORG_LIST, $e->getMessage());
            $this->flash->addMessage(RouteRegistry::CHORG_LIST, "Record not found");
            return $response->withRedirect(RouteRegistry::CHORG_LIST);
        }
    } else {
        $this->flash->addMessage(RouteRegistry::HOME, "No ID provided.");
        return $response->withRedirect(RouteRegistry::HOME);
    }
    $flash = $this->flash->getFirstMessage(RouteRegistry::CHORG_EDIT);

    $page = new CHOrgEditPage($user, "PUT", json_encode($chorg_edit) , $flash);
    return $response->write($page->render());
})->setName(RouteRegistry::CHORG_EDIT);

//$app->post(RouteRegistry::CHORG_EDIT, function (Request $request, Response $response, array $args) {
//    $all_vars = $request->getParsedBody();
//    $user = $this->session->get(SessionKeys::USER_BM);
//    return $response->write("wasaaapenin" . $all_vars);
//})->setName(RouteRegistry::CHORG_EDIT);
