<?php

use Slim\Http\Request;
use Slim\Http\Response;
use CTAF\Model\View\Partials\LoggedInWrapper;
use CTAF\DAO\Profile2DAO;
use CTAF\Controllers\HomePage;
use CTAF\Model\Report;
use CTAF\Library\Reports\Generator;
use CTAF\Model\Profile;
use CTAF\DAO\ProfileTypeDAO;
use CTAF\Controllers\ReportsPage;

//define("DIR_SEP", DIRECTORY_SEPARATOR);
//define("MIN_YEAR", 2016);

use JsonSchema\SchemaStorage;
use JsonSchema\Validator;
use JsonSchema\Constraints\Factory;


$app->get(RouteRegistry::TESTSCHEMA, function (Request $request, Response $response, array $args) {

    $hProfile2DAO = new Profile2DAO();
    $profileId = $args['profileId'];

    // "LP7034a35e-9127-9d50-f744-48845b52b4fa"

   $results = $hProfile2DAO->validateSchemaBy([
       "profileId" => $profileId
   ]);

    echo "<PRE>";

    if (empty($results)) {
        echo "The supplied JSON validates against the schema.\n";
    } else {
        echo "JSON does not validate. Violations:\n";
        echo print_r($results,true);
    }
    echo "</PRE>";
});
