<?php

namespace CTAF\Controllers;

use CTAF\Controllers\Template\CmsQaid;
use CTAF\Controllers\Template\StampTemplate;
use CTAF\DAO\ProfileSearchResults;
use CTAF\Model\UserBM;
use CTAF\Model\UserRole;

class HomePage implements StampTemplate
{
    private $user;

    public function __construct(UserBM $user)
    {
        $this->user = $user;
    }

    public $flash;

    public function render(): string
    {
        $tpl = new CmsQaid($this->user);
        $tpl->setTitle('Welcome to CTAF LMS');
//        $tpl->addCutBlock(CmsQaid::JS_DATA_TABLES);
//        $tpl->addCutBlock(CmsQaid::CSS_DATA_TABLES);
//        $tpl->addCutBlock(CmsQaid::JS_RCC);
        // show admin stuff in the mean time
        if ($this->flash) {
            $tpl->showFlash($this->flash);
        }
        $tpl->setContent("");
        if ($this->user->inRole(UserRole::ADMIN)) {
            $tpl->toggleAdminLink();
            $tpl->showCmsManage();
        }
        if ($this->user->inRole(UserRole::PSR)) {
            $tpl->showCmsManage();
        }
//        $tpl->showProfileSearch($this->search_string, $this->results);
        return $tpl->render();
    }
}