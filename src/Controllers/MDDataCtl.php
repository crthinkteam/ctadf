<?php

use Slim\Http\Request;
use Slim\Http\Response;
use CTAF\DAO\CHOrgDAO;
use CTAF\DAO\AreasDAO;
use CTAF\DAO\MDDAO;
use CTAF\Model\UserRole;
use CTAF\Model\CHOrgBM;

$app->any(RouteRegistry::MDS_DATA_P, function (Request $request, Response $response, array $args) {
    $all_vars = $request->getParsedBody();
    $requestMethod = $all_vars["methodMeta"];
    unset($all_vars["methodMeta"]);
    $user = $this->session->get(SessionKeys::USER_BM);
    $chorgResult = (new CHOrgDAO())->getCHOrgList($user->username);
    $chorglist = [];
    foreach ($chorgResult as $the) {
        array_push($chorglist, $the[CHOrgDAO::_ID]);
    }
    foreach ($all_vars['locations'] as $val) {
        if ( !in_array($val['location'],$chorglist)) {
            $this->flash->addMessage(RouteRegistry::CHORG_EDIT, "The clinic/hospital is not allowed");
            return $response->withRedirect($_SERVER['HTTP_REFERER']);
        }
    }
    $dao = new MDDAO();
    if ($requestMethod == "PUT") {
//        // update record
//        if ((array_key_exists("_id",$all_vars)) &&
//            (!is_null($all_vars["_id"]) || ($all_vars["_id"] <> ""))) {
//            $newCHOrg->_id = $all_vars['_id'];
//            try {
//                $daoBool = $dao->updateCHOrg($newCHOrg);
//            } catch (Exception $e) {
//                $this->flash->addMessage(RouteRegistry::CHORG_EDIT, "Update failed. Please contact administrator");
//                return $response->withRedirect($_SERVER['HTTP_REFERER']);
//            }
//        } else {
//            $this->flash->addMessage(RouteRegistry::CHORG_EDIT, "Invalid record ID to be updated");
//            return $response->withRedirect($_SERVER['HTTP_REFERER']);
//        }
//        $this->flash->addMessage(RouteRegistry::CHORG_EDIT, "Successfully saved.");
//        return $response->withRedirect($_SERVER['HTTP_REFERER']);
    }
    if ($requestMethod == "POST") {
        // insert new record
        try {
            $newid = $dao->createMD($all_vars);
        } catch (Exception $e) {
            $this->flash->addMessage(RouteRegistry::MDS_LIST, "Saving failed. Please contact administrator");
            return $response->withRedirect(RouteRegistry::MDS_LIST);
        }
        $this->flash->addMessage(RouteRegistry::CHORG_EDIT, "Successfully saved.");
        return $response->withRedirect("/chorg/edit/" . $newid);
    }
    return $response;
})->setName(RouteRegistry::MDS_DATA_P);

$app->get(RouteRegistry::MDS_GET_DATA, function (Request $request, Response $response, array $args) {

    if (array_key_exists('_id',$args) && ($args['_id'] <> "")) {
        try {
            $chorg_edit = (new CHOrgDAO())->getCHOrg($args['_id']);
        } catch (Exception $e) {
            $this->flash->addMessage(RouteRegistry::HOME, $e->getMessage());
            return $response->withRedirect(RouteRegistry::CHORG_LIST);
        }
    } else {
        $this->flash->addMessage(RouteRegistry::HOME, "No ID provided.");
        return $response->withRedirect(RouteRegistry::CHORG_LIST);
    }
    $response = $response->withHeader('Content-type', 'application/json');
    if (property_exists($chorg_edit,'data')) {
        unset($chorg_edit->data);
    }

    return $response->write(json_encode( $chorg_edit, JSON_PRETTY_PRINT));
})->setName(RouteRegistry::MDS_GET_DATA);


$app->get(RouteRegistry::MDS_LIST_DATA, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    if ($user->inRole(UserRole::ADMIN)) {
        $results = (new CHOrgDAO())->getCHOrgList();
    } else {
        $results = (new CHOrgDAO())->getCHOrgList($user->userName());
    }
    $response = $response->withHeader('Content-type', 'application/json');
    return $response->write(json_encode($results, JSON_PRETTY_PRINT));
})->setName(RouteRegistry::MDS_LIST_DATA);
