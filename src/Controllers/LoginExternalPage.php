<?php

namespace CTAF\Controllers;

use CTAF\Controllers\Template\LoginTemplate;
use CTAF\Controllers\Template\StampTemplate;
use CTAF\DAO\UserDAO;
use CTAF\Model\UserBM;

class LoginExternalPage implements StampTemplate
{
    const FORM_USERNAME = UserDAO::USERNAME;
    const FORM_PASSWORD = UserDAO::PASSWORD;

    private $flash_message;

    public function __construct($msg)
    {
        $this->flash_message = $msg;
    }

    public function render(): string
    {
        $tpl = new LoginTemplate();
        $tpl->showFlash($this->flash_message);
        return $tpl->render();
    }
}