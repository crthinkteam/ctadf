<?php

namespace CTAF\Controllers;

use CTAF\Controllers\Template\BfdCmsWrapper;
use CTAF\Controllers\Template\StampTemplate;
use function MongoDB\BSON\toPHP;
use StampTemplateEngine\StampTE;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\TwitterBootstrap3View;

class UsersPage implements StampTemplate
{

    const TEMPLATE_FILE = StampTemplate::TEMPLATE_DIR . 'users.html';
    const SEARCH_STRING = 'search_string';
    private $user;
    private $flash;
    private $search_string;
    private $results;
    private $pageno = 1;

    public function __construct($user, $flash, $searchString, $results, $pageno = 1)
    {
        $this->user = $user;
        $this->flash = $flash;
        $this->search_string = $searchString;
        $this->results = $results;
        $this->pageno = $pageno;
    }

    private function _render(): string
    {
        $stamp = new StampTE(file_get_contents(static::TEMPLATE_FILE));
        $content = $stamp->get('content');
        $content->inject(self::SEARCH_STRING, self::SEARCH_STRING);
        if ($this->search_string !== '') {
            $content->inject('search_string_value', $this->search_string);
        }
        $content->inject('link_search_get', \RouteRegistry::USERS);

        $adapter = new ArrayAdapter($this->results);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(10);
        $pagerfanta->setCurrentPage($this->pageno);

        $view = new TwitterBootstrap3View();
        $options = array('proximity' => 10);
        $route_generator = function ($pageno) {
            $route = \RouteRegistry::USERS;
            $search_attr = self::SEARCH_STRING;
            return "{$route}?pageno={$pageno}&{$search_attr}={$this->search_string}";
        };
        $html = $view->render($pagerfanta, $route_generator, $options);
        $content->injectRaw('pagerlinkstext', $html);

        foreach ($pagerfanta->getCurrentPageResults() as $u) {
            $res_blk = $content->get('result_entry')->copy();
            $username = $u['userName'];
            $res_blk->inject('username', $u['userName']);
            $res_blk->inject('last_name', $u['name']['familyName']);
            $res_blk->inject('first_name', $u['name']['givenName']);
            $email = trim($u['email']);
            if(empty($email)){
                $res_blk->inject('email', 'Not Set');
            } else {
                $res_blk->inject('email', $email);
            }
            $active = isset($u['active'])?$u['active']:false;
            if($active){
                $res_blk->inject('active','Yes');
            } else {
                $res_blk->inject('active','No');
            }
            $password = isset($u['password'])?$u['password']:"";
            if(empty($password)){
                $res_blk->inject('password','Not Set');
            } else {
                $res_blk->inject('password','Set');
            }
            $res_blk->inject('link_user_edit', \RouteRegistry::USER . $username);
            $r = $u['roles'];
            if (!is_string($r) && $r !== null) {
                $r = implode(', ', $r->bsonSerialize());
            }
            $res_blk->inject('roles', $r);
            $content->glue('result_entry_paste', $res_blk);
        }
        return $content;
    }

    public function render(): string
    {
        $tpl = new BfdCmsWrapper(
            $this->user,
            'All Users',
            'Users',
            $this->_render(),
            [],
            $this->flash
        );
        return $tpl->render();
    }
}
