<?php

use Slim\Http\Request;
use Slim\Http\Response;
use CTAF\DAO\CHOrgDAO;
use CTAF\DAO\AreasDAO;
use CTAF\Model\UserRole;
use CTAF\Model\CHOrgBM;

$app->any(RouteRegistry::CHORG_DATA_P, function (Request $request, Response $response, array $args) {
    $all_vars = $request->getParsedBody();
    $requestMethod = $all_vars["method"];
    unset($all_vars["method"]);
    $areas = (new AreasDAO())->allAreasList();
    $dao = new CHOrgDAO();
    $newCHOrg = new CHOrgBM($all_vars);

    if ( !in_array($all_vars[CHOrgBM::AREA_NAME],$areas)) {
        $this->flash->addMessage(RouteRegistry::CHORG_EDIT, "The area is not allowed");
        return $response->withRedirect($_SERVER['HTTP_REFERER']);
    }
    if ( !in_array($all_vars[CHOrgBM::ORG_TYPE],CHOrgBM::ORG_TYPE_LIST)) {
        $this->flash->addMessage(RouteRegistry::CHORG_EDIT, "The clinic/hospital type value is not allowed");
        return $response->withRedirect($_SERVER['HTTP_REFERER']);
    }

    if ($requestMethod == "PUT") {
        if ((array_key_exists("_id",$all_vars)) &&
            (!is_null($all_vars["_id"]) || ($all_vars["_id"] <> ""))) {
            $newCHOrg->_id = $all_vars['_id'];
            try {
                $daoBool = $dao->updateCHOrg($newCHOrg);
            } catch (Exception $e) {
                $this->flash->addMessage(RouteRegistry::CHORG_EDIT, "Update failed. Please contact administrator");
                return $response->withRedirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            $this->flash->addMessage(RouteRegistry::CHORG_EDIT, "Invalid record ID to be updated");
            return $response->withRedirect($_SERVER['HTTP_REFERER']);
        }
        $this->flash->addMessage(RouteRegistry::CHORG_EDIT, "Successfully saved.");
        return $response->withRedirect($_SERVER['HTTP_REFERER']);
    }
    if ($requestMethod == "POST") {
        try {
            $newid = $dao->createCHOrg($newCHOrg);
        } catch (Exception $e) {
            $this->flash->addMessage(RouteRegistry::CHORG_LIST, "Saving failed. Please contact administrator");
            return $response->withRedirect(RouteRegistry::CHORG_LIST);
        }
        $this->flash->addMessage(RouteRegistry::CHORG_EDIT, "Successfully saved.");
        return $response->withRedirect("/chorg/edit/" . $newid);

    }
})->setName(RouteRegistry::CHORG_DATA_P);

$app->get(RouteRegistry::CHORG_GET_DATA, function (Request $request, Response $response, array $args) {

    if (array_key_exists('_id',$args) && ($args['_id'] <> "")) {
        try {
            $chorg_edit = (new CHOrgDAO())->getCHOrg($args['_id']);
        } catch (Exception $e) {
            $this->flash->addMessage(RouteRegistry::HOME, $e->getMessage());
            return $response->withRedirect(RouteRegistry::CHORG_LIST);
        }
    } else {
        $this->flash->addMessage(RouteRegistry::HOME, "No ID provided.");
        return $response->withRedirect(RouteRegistry::CHORG_LIST);
    }
    $response = $response->withHeader('Content-type', 'application/json');
    if (property_exists($chorg_edit,'data')) {
        unset($chorg_edit->data);
    }

    return $response->write(json_encode( $chorg_edit, JSON_PRETTY_PRINT));
})->setName(RouteRegistry::CHORG_GET_DATA);


$app->get(RouteRegistry::CHORG_LIST_DATA, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    if ($user->inRole(UserRole::ADMIN)) {
        $results = (new CHOrgDAO())->getCHOrgList();
    } else {
        $results = (new CHOrgDAO())->getCHOrgList($user->username);
    }
    $response = $response->withHeader('Content-type', 'application/json');
    return $response->write(json_encode($results, JSON_PRETTY_PRINT));
})->setName(RouteRegistry::CHORG_LIST_DATA);
