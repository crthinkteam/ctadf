<?php

use CTAF\Model\UserRole;
use Slim\Http\Request;
use Slim\Http\Response;
use MongoDB\BSON\Regex;
use CTAF\DAO\AreasDAO;
use CTAF\Model\AreasBM;


$app->get(RouteRegistry::AREAS_DATA, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    if ($user->inRole(UserRole::ADMIN)) {
        $results = (new AreasDAO())->findBy([]);
    } else {
        $results = (new AreasDAO())->getUserAreas($user->userName());
    }
//        $results = (new AreasDAO())->findBy(['psrName'=> $user->userName()]);
    $response = $response->withHeader('Content-type', 'application/json');
    return $response->write(json_encode($results, JSON_PRETTY_PRINT));
})->setName(RouteRegistry::AREAS_DATA);

$app->post(RouteRegistry::AREAS_DATA_P, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    $parsed_body = $request->getParsedBody();
    $dao = new AreasDAO();
    $area = new AreasBM();
    $key2run = 'areaName';
    if (array_key_exists($key2run,$parsed_body)) {
        $area->$key2run = $parsed_body[$key2run];
    }

    $key2run = 'psrName';
    if ($user->inRole(UserRole::ADMIN)) {
        if (is_array($parsed_body) && array_key_exists($key2run,$parsed_body)) {
            $area->$key2run = $parsed_body[$key2run];
        }
    } else if ($user->inRole(UserRole::PSR)) {
        $area->$key2run = $user->userName();
    } else {
        $response->withStatus(500);
    }

    if ((array_key_exists("_id",$parsed_body)) &&
        (!is_null($parsed_body["_id"]) || ($parsed_body["_id"] <> ""))) {
        $area->_id = $parsed_body['_id'];
        $daoBool = $dao->updateArea($area);
    } else if(!(($area->areaName <> "") && ($area->psrName <> ""))) {
        $daoBool = $dao->createArea($area);
    }
    return $response;
})->setName(RouteRegistry::AREAS_DATA_P);


//$app->post(RouteRegistry::AREAS_DELETE, function (Request $request, Response $response, array $args) {
//    $parsed_body = $request->getParsedBody();

$app->post(RouteRegistry::AREAS_DELETE, function (Request $request, Response $response, array $args) {
$parsed_body = $request->getParsedBody();
// TODO: restrict to user owned record only
     foreach ($parsed_body as $ad) {
        $dao = new AreasDAO();
        $dao->deleteOne($ad["_id"]);
    }

    return $response;
})->setName(RouteRegistry::AREAS_DELETE);
