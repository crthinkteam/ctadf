<?php

namespace CTAF\Controllers;


use CTAF\Controllers\Template\CmsQaid;
use CTAF\DAO\AreasDAO;
use CTAF\DAO\CHOrgDAO;
use CTAF\DAO\ContactInfoTypeDAO;
use CTAF\DAO\LookupDAO;
use CTAF\DAO\ProductDAO;
use CTAF\DAO\SpecialtiesDAO;
use CTAF\Model\CHOrgBM;
use CTAF\Model\UserRole;
use StampTemplateEngine\StampTE;
use CTAF\Controllers\Template\StampTemplate;
use CTAF\Model\UserBM;

class MDEditPage implements StampTemplate
{

    const TEMPLATE_FILE = StampTemplate::TEMPLATE_DIR . 'md-edit.html';

    private $_id;
    private $user;
    private $flash;
    private $stamp;
    private $customJS_ST;
    private $dataedit;
    private $formMethod;

    public function __construct(UserBM $user, $method, string $dataedit = "", $flash=null)
    {
        $this->flash =$flash;
        $this->user = $user;
        $this->dataedit = $dataedit;
        $this->formMethod = $method;
        $this->stamp = new StampTE(file_get_contents(static::TEMPLATE_FILE));
    }


    public function showFlash(string $msg)
    {
        $flash = $this->stamp->get('content.flash');
        if(!empty($msg)){
            $flash->injectRaw('msg', $msg);
        } else {
            $flash->inject('msg', "");
        }
        $this->stamp->add($flash);
    }

    private function lookupListMap( $set) : array {
        $lookupResult = (new LookupDAO())->getLookupList($set);
        $lookuplist = [];
        $lookupIDlist = [];
        foreach ($lookupResult as $rk => $v){
            $lookuplist[$v['_id']] = $v['value'];
            $lookupIDlist[] = $v['_id'];
        }
        return [$lookuplist,$lookupIDlist];
    }

    private function _render(): string
    {
        $content = $this->stamp->get('content');
        $jschorg = $this->stamp->get('js_dtchorg');

        //set values in json form, jsblock

        // chorg lookup
        $results = (new CHOrgDAO())->getCHOrgList($this->user->userName());
        $chorgmaplist = [];
        $chorgidlist = [];
        foreach ($results as $rk => $v){
            $chorgmaplist[$v['_id']] = $v['chorgName'] . ", " . $v['areaName'];
            $chorgidlist[] = $v['_id'];
        }
        $jschorg->injectRaw("chorgmaplist",json_encode($chorgmaplist));
        $jschorg->injectRaw("chorgidlist",json_encode($chorgidlist));

        // info type lookup
        list($contactinfotypelist, $contactinfoIDlist) = $this->lookupListMap("infotype");
        $jschorg->injectRaw("contactinfotypelist",json_encode($contactinfotypelist));
        $jschorg->injectRaw("contactinfoIDlist",json_encode($contactinfoIDlist));

        // product list lookup
        list($productNameList, $productIDlist) = $this->lookupListMap("products");
        $jschorg->injectRaw("productnamelist",json_encode($productNameList));
        $jschorg->injectRaw("productidlist",json_encode($productIDlist));

        // specialty list lookup
        list($specialtiesNameList, $specialtiesIDlist) = $this->lookupListMap("specialties");
        $jschorg->injectRaw("specialtiesnamelist",json_encode($specialtiesNameList));
        $jschorg->injectRaw("specialtiesidlist",json_encode($specialtiesIDlist));

        // subspecialty list lookup
        list($subspecialtiesNameList, $subspecialtiesIDlist) = $this->lookupListMap("subspecialties");
        $jschorg->injectRaw("subspecialtiesnamelist",json_encode($subspecialtiesNameList));
        $jschorg->injectRaw("subspecialtiesidlist",json_encode($subspecialtiesIDlist));

        // potential list lookup
        list($potentialList, $potentialIDlist) = $this->lookupListMap("potential");
        $jschorg->injectRaw("potentialList",json_encode($potentialList));
        $jschorg->injectRaw("potentialIDlist",json_encode($potentialIDlist));

        // dataedit is array, filled or empty, passed into the class
        if (strlen($this->dataedit) > 2) {
            $jschorg->injectRaw("jsonvaluesphp",$this->dataedit);
        } else {
            $jschorg->injectRaw("jsonvaluesphp","{}");
        }
        $jschorg->injectRaw("formmethod",$this->formMethod);
//        $jschorg->injectRaw("jsonvalues",$this->dataedit);

        $this->customJS_ST = $jschorg;
        return $content;
    }

    public function renderBFD(): string
    {
        $full_name = "{$this->user_edit->givenName} {$this->user_edit->familyName}";
        $tpl = new BfdCmsWrapper(
            $this->user,
            "Editing user: {$full_name}",
            "Editing user: {$full_name}",
            $this->_render(),
            [BfdCmsWrapper::JS_I_CHECK, BfdCmsWrapper::CSS_I_CHECK, BfdCmsWrapper::JS_RED_CONNECT],
            $this->flash
        );
        return $tpl->render();
    }

    public function render(): string
    {
        $tpl = new CmsQaid($this->user);
        $tpl->setTitle('Edit Clinic/Hospital/Org');

        if ($this->flash) {
            $tpl->showFlash($this->flash);
        }
        if ($this->user->inRole(UserRole::ADMIN)) {
            $tpl->toggleAdminLink();
        }
        if ($this->user->inRole(UserRole::PSR)) {
        }
        if ($this->user->inRole(UserRole::ADMIN) ||
            $this->user->inRole(UserRole::PSR)) {
            $tpl->setContent($this->_render());

            $tpl->addCustomScriptBlock($this->customJS_ST);
        }

        return $tpl->render();
    }
}