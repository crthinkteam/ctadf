<?php

use Slim\Http\Request;
use Slim\Http\Response;
use CTAF\DAO\CHORgDAO;
use CTAF\Model\UserRole;

$app->post(RouteRegistry::CHORG_DATA_P, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    $parsed_body = $request->getParsedBody();
//    $dao = new CHOrgDAO();
//    $newCHOrg = new CHOrgBM();
//
//    $fieldsCHOrg = [
//        CHOrgBM::NAME,
//        CHOrgBM::ADDRESS,
//        CHOrgBM::LANDLINE,
//        CHOrgBM::ORG_TYPE,
//    ];
//    foreach ($fieldsCHOrg as $key) {
//        if (array_key_exists($key,$parsed_body)) {
//            $newCHOrg->$key = $parsed_body[$key];
//        }
//    }
//
//    if ((array_key_exists("_id",$parsed_body)) &&
//        (!is_null($parsed_body["_id"]) || ($parsed_body["_id"] <> ""))) {
//        $newCHOrg->_id = $parsed_body['_id'];
//        $daoBool = $dao->updateCHOrg($newCHOrg);
//    } else if(!(
//        ($newCHOrg->{CHOrgBM::NAME} <> "") &&
//        ($newCHOrg->{CHOrgBM::ADDRESS} <> "") &&
//        ($newCHOrg->{CHOrgBM::LANDLINE} <> "") &&
//        ($newCHOrg->{CHOrgBM::ORG_TYPE} <> "")
//    )) {
//        $daoBool = $dao->createCHOrg($newCHOrg);
//    }
    return $response->write("wasaaapenin" . $parsed_body);
})->setName(RouteRegistry::CHORG_DATA_P);

$app->get(RouteRegistry::CHORG_GET_DATA, function (Request $request, Response $response, array $args) {

    if (array_key_exists('_id',$args) && ($args['_id'] <> "")) {
        try {
            $chorg_edit = (new CHOrgDAO())->getCHOrg($args['_id']);
        } catch (Exception $e) {
            $this->flash->addMessage(RouteRegistry::HOME, $e->getMessage());
            return $response->withRedirect(RouteRegistry::CHORG_LIST);
        }
    } else {
        $this->flash->addMessage(RouteRegistry::HOME, "No ID provided.");
        return $response->withRedirect(RouteRegistry::CHORG_LIST);
    }
    $response = $response->withHeader('Content-type', 'application/json');
    if (property_exists($chorg_edit,'data')) {
        unset($chorg_edit->data);
    }

    return $response->write(json_encode( $chorg_edit, JSON_PRETTY_PRINT));
})->setName(RouteRegistry::CHORG_GET_DATA);


$app->get(RouteRegistry::CHORG_LIST_DATA, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    if ($user->inRole(UserRole::ADMIN)) {
        $results = (new CHOrgDAO())->getCHOrgList();
    } else {
        $results = (new CHOrgDAO())->getCHOrgList($user->userName());
    }
    $response = $response->withHeader('Content-type', 'application/json');
    return $response->write(json_encode($results, JSON_PRETTY_PRINT));
})->setName(RouteRegistry::CHORG_LIST_DATA);


