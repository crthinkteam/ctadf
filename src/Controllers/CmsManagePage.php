<?php

namespace CTAF\Controllers;

use CTAF\Controllers\Template\CmsQaid;
use CTAF\Controllers\Template\StampTemplate;
use CTAF\DAO\ProfileSearchResults;
use CTAF\Model\LookupBM;
use CTAF\Model\LookupBMArray;
use CTAF\Model\AreasBMArray;
use CTAF\Model\UserBM;

class CmsManagePage implements StampTemplate
{
    private $user;
    private $areas;
    private $lookups;

//    public function __construct(UserBM $user, AreasBMArray $areas, LookupBMArray $lookups)
    public function __construct(UserBM $user)
    {
        $this->user = $user;
    }

    public function render(): string
    {
        $tpl = new CmsQaid($this->user);
        $tpl->setTitle('Manage CMS');
        if ($this->flash) {
            $tpl->showFlash($this->flash);
        }
        $tpl->setContent("");
        $tpl->toggleAdminLink();
        $tpl->showCmsAdmin();
        return $tpl->render();
    }
}