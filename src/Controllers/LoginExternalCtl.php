<?php

use Slim\Http\Request;
use Slim\Http\Response;

use CTAF\Model\UserBM;
use CTAF\DAO\UserDAO;

use CTAF\Controllers\LoginExternalPage;
//use BFD\Controllers\LoginExternalPageForm;

$app->get(RouteRegistry::LOGIN_EXTERNAL, function (Request $request, Response $response, array $args) {
    if ($this->session->exists(SessionKeys::USER_BM)) {
        return $response->withRedirect(RouteRegistry::HOME);
    }
    $flash = $this->flash->getFirstMessage(RouteRegistry::LOGIN_EXTERNAL);
    $page = new LoginExternalPage($flash);
    return $response->write($page->render());
})->setName(RouteRegistry::LOGIN_EXTERNAL);

$app->post(RouteRegistry::LOGIN_EXTERNAL_P, function (Request $request, Response $response, array $args) {
    $all_vars = $request->getParsedBody();
    $username = trim($all_vars['username']);
    $password = trim($all_vars['password']);
    try {
        $user = (new UserDAO())->login($username);
    } catch (Exception $e) {
        $this->flash->addMessage(RouteRegistry::LOGIN_EXTERNAL, $e->getMessage());
        return $response->withRedirect(RouteRegistry::LOGIN_EXTERNAL);
    }
    if(md5($password) !== $user->password) {
        $this->flash->addMessage(RouteRegistry::LOGIN_EXTERNAL, "Wrong username and or password");
        $redir_route = RouteRegistry::LOGIN_EXTERNAL;
    } else {
        $this->session->set(SessionKeys::USER_BM, $user);
        $this->session->set(SessionKeys::LOGIN_ROUTE, RouteRegistry::LOGIN_EXTERNAL);
        $redir_route = RouteRegistry::HOME;
    }
    return $response->withRedirect($redir_route);
})->setName(RouteRegistry::LOGIN_EXTERNAL_P);
