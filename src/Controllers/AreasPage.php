<?php

namespace CTAF\Controllers;

use CTAF\Controllers\Template\BfdCmsWrapper;
use CTAF\Controllers\Template\CmsQaid;
use CTAF\Controllers\Template\StampTemplate;
use CTAF\Model\UserRole;
use function MongoDB\BSON\toPHP;
use StampTemplateEngine\StampTE;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\TwitterBootstrap3View;

class AreasPage implements StampTemplate
{

    const TEMPLATE_FILE = StampTemplate::TEMPLATE_DIR . 'areas.html';
    const TEMPLATE_FILE_PSR = StampTemplate::TEMPLATE_DIR . 'areas-psr.html';
    const SEARCH_STRING = 'search_string';
    private $user;
    private $flash;
    private $search_string;
    private $results;
    private $pageno = 1;
    private $areasJSBlock;
    private $stamp;

    public function __construct($user, $flash, $results, $pageno = 1)
    {
        $this->user = $user;
        $this->flash = $flash;
        $this->results = $results;
        $this->pageno = $pageno;
    }

    private function _render(): string
    {
        if ($this->user->inRole(UserRole::ADMIN))
            $this->stamp = new StampTE(file_get_contents(static::TEMPLATE_FILE));
        else
            $this->stamp = new StampTE(file_get_contents(static::TEMPLATE_FILE_PSR));
        $content = $this->stamp->get('content');
//        $areasJSBlock = $stamp->get('js_dtfh');
        $this->areasJSBlock = ($this->stamp->get('js_dtfh'))->getString();

        return $content;
    }

    public function renderBFD(): string
    {
        if ($this->user->inRole(UserRole::ADMIN))
            $titlePageHead = "PSR Areas of xxx";
        else
            $titlePageHead = "Areas";

        $tpl = new BfdCmsWrapper(
            $this->user,
            $titlePageHead,
            $titlePageHead,
            $this->_render(),
            [],
            $this->flash
        );
        $tabulatorcss = $tpl->get('tabulator_css');
        $tpl->add($tabulatorcss);
        $tpl->addCustomScriptBlock($this->areasJSBlock);
        return $tpl->render();
    }

    public function render(): string
    {
        $tpl = new CmsQaid($this->user);
        if ($this->user->inRole(UserRole::ADMIN)) {
            $tpl->setTitle('PSR Areas of xxx');
        } else {
            $tpl->setTitle('Areas');
        }
        if ($this->flash) {
            $tpl->showFlash($this->flash);
        }
        if ($this->user->inRole(UserRole::ADMIN)) {
            $tpl->toggleAdminLink();
        }
        if ($this->user->inRole(UserRole::PSR)) {
        }
        if ($this->user->inRole(UserRole::ADMIN) ||
            $this->user->inRole(UserRole::PSR)) {
            $tpl->setContent($this->_render());
            $tabulatorcss = $this->stamp->get('tabulator_css');
            $tpl->add($tabulatorcss);
            $tpl->addCustomScriptBlock($this->areasJSBlock);
        }

        return $tpl->render();
    }
}
