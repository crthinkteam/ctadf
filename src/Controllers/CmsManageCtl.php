<?php

use CTAF\Model\UserRole;
use Slim\Http\Request;
use Slim\Http\Response;
use CTAF\Controllers\CmsManagePage;
use CTAF\DAO\AreasDAO;
//use CTAF\DAO\LookupDAO;

$app->get(RouteRegistry::CMS_MANAGE, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    if (!$user->inRole(UserRole::ADMIN)) {
        return $response->withRedirect(RouteRegistry::HOME);
    }

//    $areas = (new AreasDAO())->all();
//    $lookups = (new LookupDAO(LookupDAO::REGISTRY_COLLECTION))->all();
//    $profile_types = null;
    $lookups = null;
    $page = new CmsManagePage($user);
    return $response->write($page->render());
})->setName(RouteRegistry::CMS_MANAGE);
