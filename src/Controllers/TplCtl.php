<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->get(RouteRegistry::TEMPLATE_LOGIN, function (Request $request, Response $response, array $args) {
    $tpl = file_get_contents(__DIR__ . "/../../public/templates/login.html");
    return $response->write($tpl);
})->setName(RouteRegistry::TEMPLATE_LOGIN);

//$app->get(RouteRegistry::TEMPLATE_CMS, function (Request $request, Response $response, array $args) {
//    $tpl = file_get_contents(__DIR__ . "/../../public/templates/cms.html");
//    return $response->write($tpl);
//})->setName(RouteRegistry::TEMPLATE_CMS);
//
//$app->get(RouteRegistry::TEMPLATE_CMS_WORKING, function (Request $request, Response $response, array $args) {
//    $tpl = file_get_contents(__DIR__ . "/../../public/templates/cms-working.html");
//    return $response->write($tpl);
//})->setName(RouteRegistry::TEMPLATE_CMS_WORKING);
//
//$app->get(RouteRegistry::TEMPLATE_CMS_QAID, function (Request $request, Response $response, array $args) {
//    $tpl = file_get_contents(__DIR__ . "/../../public/templates/cms-qaid.html");
//    return $response->write($tpl);
//})->setName(RouteRegistry::TEMPLATE_CMS_QAID);
//
//$app->get(RouteRegistry::TEMPLATE_CMS_QAID_TEST_RENDER, function (Request $request, Response $response, array $args) {
//    $tpl = new \BFD\Controllers\Template\CmsQaid();
//    return $response->write($tpl->testRenderNotWide());
//})->setName(RouteRegistry::TEMPLATE_CMS_QAID_TEST_RENDER);
//
//$app->get(RouteRegistry::TEMPLATE_CMS_QAID_TEST_RENDER_WIDE, function (Request $request, Response $response, array $args) {
//    $tpl = new \BFD\Controllers\Template\CmsQaid();
//    return $response->write($tpl->testRenderWide());
//})->setName(RouteRegistry::TEMPLATE_CMS_QAID_TEST_RENDER_WIDE);
