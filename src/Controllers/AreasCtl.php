<?php

use Slim\Http\Request;
use Slim\Http\Response;
use MongoDB\BSON\Regex;

use CTAF\Controllers\AreasPage;
use CTAF\DAO\AreasDAO;
use CTAF\Model\AreasBM;

$app->get(RouteRegistry::AREAS, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
//    $search_string = $request->getQueryParam(UsersPage::SEARCH_STRING, '');
//    if(!empty($search_string)) {
//        $results = (new UserDAO())->findBy(['userName' => new Regex($search_string, 'i')]);
//    } else {
        $results = [];
//    }
    $flash = $this->flash->getFirstMessage(RouteRegistry::AREAS);
//    $pageno = intval($request->getQueryParam('pageno', 1));
    $pageno = 1;
    $page = new AreasPage($user, $flash, $results, $pageno);
    return $response->write($page->render());
})->setName(RouteRegistry::AREAS);


//$app->get(RouteRegistry::USER_EDIT, function (Request $request, Response $response, array $args) {
//    $user = $this->session->get(SessionKeys::USER_BM);
//    $user_edit_id = $args['_id'];
//    $user_edit = (new UserDAO())->getUser($user_edit_id);
//    $flash = $this->flash->getFirstMessage(RouteRegistry::USER_EDIT);
//    $page = new UserEditPage($user, $user_edit, $flash);
//    return $response->write($page->render());
//})->setName(RouteRegistry::USER_EDIT);
//
//$app->post(RouteRegistry::USER_EDIT_P, function (Request $request, Response $response, array $args) {
//    $parsed_body = $request->getParsedBody();
//    $dao = new UserDAO();
//    $username = $parsed_body['username'];
//    $user_edit = $dao->getUser($username);
//    $user_edit->roles = $parsed_body['roles'];
//    $user_edit->email = $parsed_body['email'];
//    $user_edit->title = $parsed_body['title'];
//    $user_edit->givenName = $parsed_body['first_name'];
//    $user_edit->middleName = $parsed_body['middle_name'];
//    $user_edit->familyName = $parsed_body['last_name'];
//    $pw1 = $parsed_body['password'];
//    $pw2 = $parsed_body['password2'];
//    $active = $parsed_body['active'];
//    if($active === "active"){
//        $user_edit->active=true;
//    } else {
//        $user_edit->active=false;
//    }
//    $flashmessage= "";
//    if(!empty($pw1)){
//        if($pw1 === $pw2) {
//            $user_edit->password = md5($pw1);
//            $flashmessage = "The users password has also been updated";
//        } else {
//            $this->flash->addMessage(RouteRegistry::USER_EDIT, "The two copies of the password you have supplied do not match");
//            $redir_route = "/user/" . $username;
//            return $response->withRedirect($redir_route);
//        }
//    }
//    $dao->updateUser($user_edit);
//    $this->flash->addMessage(RouteRegistry::USER_EDIT, "The user record has been updated. " . $flashmessage);
//    return $response->withRedirect(RouteRegistry::USER . $username);
//})->setName(RouteRegistry::USER_EDIT_P);
//
//$app->get(RouteRegistry::USER_NEW, function (Request $request, Response $response, array $args) {
//    $user = $this->session->get(SessionKeys::USER_BM);
//    $flash = $this->flash->getFirstMessage(RouteRegistry::USER_EDIT);
//    $page = new UserNewPage($user,$flash);
//    return $response->write($page->render());
//})->setName(RouteRegistry::USER_NEW);
//
//$app->post(RouteRegistry::USER_NEW_P, function (Request $request, Response $response, array $args) {
//    $parsed_body = $request->getParsedBody();
//    $dao = new UserDAO();
//    $username = $parsed_body['username'];
//    $user_new = new UserBM([]);
//    $user_new->username = $username;
//    $user_new->roles = $parsed_body['roles'];
//    $user_new->email = $parsed_body['email'];
//    $user_new->title = $parsed_body['title'];
//    $user_new->givenName = $parsed_body['first_name'];
//    $user_new->middleName = $parsed_body['middle_name'];
//    $user_new->familyName = $parsed_body['last_name'];
//    $pw1 = $parsed_body['password'];
//    $pw2 = $parsed_body['password2'];
//    $active = $parsed_body['active'];
//    if($active === "active"){
//        $user_new->active=true;
//    } else {
//        $user_new->active=false;
//    }
//    $flashmessage= "";
//    if(!empty($pw1)){
//        if($pw1 === $pw2) {
//            $user_new->password = md5($pw1);
//            $flashmessage = "The users password has been updated";
//        } else {
//            $this->flash->addMessage(RouteRegistry::USER_EDIT, "The two copies of the password you have supplied do not match");
//            $redir_route = RouteRegistry::USER_NEW;
//            return $response->withRedirect($redir_route);
//        }
//    }
//    $dao->createUser($user_new);
//    $this->flash->addMessage(RouteRegistry::USER_EDIT, "The user record has been created. " . $flashmessage);
//    return $response->withRedirect(RouteRegistry::USER . $username);
//})->setName(RouteRegistry::USER_NEW_P);
