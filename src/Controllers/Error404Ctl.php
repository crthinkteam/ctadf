<?php

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Body;
use CTAF\Controllers\ErrorPage;

$app->get(RouteRegistry::ERROR404, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    $page = new ErrorPage($user,404);
    $flash = $this->flash->getFirstMessage(RouteRegistry::ERROR404);
    if ($flash) {
        $page->flash = $flash;
    }
    return $response->write($page->render())->withStatus(404);
})->setName(RouteRegistry::ERROR404);

$app->get(RouteRegistry::ERROR403, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    $page = new ErrorPage($user,403);
    $flash = $this->flash->getFirstMessage(RouteRegistry::ERROR403);
    if ($flash) {
        $page->flash = $flash;
    }
    return $response->write($page->render())->withStatus(403);
})->setName(RouteRegistry::ERROR403);

$app->get(RouteRegistry::ERROR500, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    $page = new ErrorPage($user,500);
    $flash = $this->flash->getFirstMessage(RouteRegistry::ERROR403);
    if ($flash) {
        $page->flash = $flash;
    }
    return $response->write($page->render())->withStatus(500);
})->setName(RouteRegistry::ERROR500);

