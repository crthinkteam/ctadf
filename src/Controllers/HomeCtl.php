<?php

use Slim\Http\Request;
use Slim\Http\Response;
use CTAF\Controllers\HomePage;
use CTAF\DAO\Profile2DAOSlave;

$app->get(RouteRegistry::HOME, function (Request $request, Response $response, array $args) {
    $user = $this->session->get(SessionKeys::USER_BM);
    $page = new HomePage($user);
    $flash = $this->flash->getFirstMessage(RouteRegistry::HOME);
    if ($flash) {
        $page->flash = $flash;
    }
    return $response->write($page->render());
});
