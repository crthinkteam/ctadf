<?php

namespace CTAF\Controllers;


use CTAF\Model\UserRole;
use StampTemplateEngine\StampTE;
use CTAF\Controllers\Template\StampTemplate;
use CTAF\Controllers\Template\BfdCmsWrapper;
use CTAF\Model\UserBM;

class AreaNewPage implements StampTemplate
{

    const TEMPLATE_FILE = StampTemplate::TEMPLATE_DIR . 'user-edit.html';
    const ENGLISH_ACTION_MSG = "add";
    private $user;

    private $stamp = null;
    private $flash = null;

    public function __construct(UserBM $user, $flash=null)
    {
        $this->flash =$flash;
        $this->user = $user;

        $this->stamp =  new StampTE(file_get_contents(static::TEMPLATE_FILE));
    }


    public function showFlash(string $msg)
    {
        $flash = $this->stamp->get('content.flash');
        if(!empty($msg)){
            $flash->injectRaw('msg', $msg);
        } else {
            $flash->inject('msg', "");
        }
        $this->stamp->add($flash);
    }

    protected function setUser(UserBM $user)
    {
        $this->stamp->inject('USERNAME', $user->userName());
        $this->stamp->inject('USER_FULL_NAME', $user->displayName());
        $this->stamp->inject('USER_TITLE', $user->title);
        if (!$user->inRole(UserRole::RESEARCHER)  && !$user->inRole(UserRole::ADMIN)) {
            $this->stamp->inject('role_is_readonly', 'true');
        }
    }



    private function _render(): string
    {

        $content = $this->stamp->get('content');
        $content->inject('button_label', 'Create');
        $content->inject('form_action', \RouteRegistry::USER_NEW_P);

        if(!empty($this->flash)){
            $this->showFlash($this->flash);
        } else {
            $this->showFlash("");
        }

        $editrolestr = "";

        $roles = [];

        foreach(UserBM::ROLESET as $rolename => $role){
            $templateFrag = "";
            if(in_array($rolename, $roles)){
                $templateFrag = "
                 <div class=\"checkbox\">
                                    <label for=\"role_editor\">
                                        <input type=\"checkbox\"
                                               id=\"role_{$rolename}\"
                                               name=\"roles[]\"
                                               value=\"{$rolename}\"
                                               class=\"margin\"
                                               checked >
                                        {$role}
                                    </label>
                                </div>
                \n";
            } else {
                $templateFrag = "
                 <div class=\"checkbox\">
                                    <label for=\"role_editor\">
                                        <input type=\"checkbox\"
                                               id=\"role_{$rolename}\"
                                               name=\"roles[]\"
                                               value=\"{$rolename}\"
                                               class=\"margin\"
                                               >
                                        {$role}
                                    </label>
                                </div>
                \n";

            }
            $editrolestr .= $templateFrag;
        }
        $content->inject('english_action_msg', static::ENGLISH_ACTION_MSG);
        $content->injectRaw("roleschecks", $editrolestr);

        return $content;
    }

    public function render(): string
    {
        $tpl = new BfdCmsWrapper(
            $this->user,
            'New User',
            'Define New User',
            $this->_render(),
            [BfdCmsWrapper::JS_I_CHECK, BfdCmsWrapper::CSS_I_CHECK, BfdCmsWrapper::JS_RED_CONNECT],
            $this->flash
        );
        return $tpl->render();
    }
}