<?php

namespace CTAF\Controllers;


use CTAF\Controllers\Template\CmsQaid;
use CTAF\DAO\AreasDAO;
use CTAF\Model\CHOrgBM;
use CTAF\Model\UserRole;
use StampTemplateEngine\StampTE;
use CTAF\Controllers\Template\StampTemplate;
use CTAF\Model\UserBM;

class CHOrgEditPage implements StampTemplate
{

    const TEMPLATE_FILE = StampTemplate::TEMPLATE_DIR . 'chorg-edit.html';

    private $_id;
    private $user;
    private $flash;
    private $stamp;
    private $customJS_ST;
    private $chorgedit;
    private $formMethod;

    public function __construct(UserBM $user, $method, string $chorgedit, $flash=null)
    {
        $this->flash =$flash;
        $this->user = $user;
        $this->chorgedit = $chorgedit;
        $this->formMethod = $method;
        $this->stamp = new StampTE(file_get_contents(static::TEMPLATE_FILE));
    }


    public function showFlash(string $msg)
    {
        $flash = $this->stamp->get('content.flash');
        if(!empty($msg)){
            $flash->injectRaw('msg', $msg);
        } else {
            $flash->inject('msg', "");
        }
        $this->stamp->add($flash);
    }

    private function _render(): string
    {
        $content = $this->stamp->get('content');
        $jschorg = $this->stamp->get('js_dtchorg');

        //set values in json form, jsblock
        $results = (new AreasDAO())->getUserAreas($this->user->userName());
        foreach ($results as $rv => $k){
            $arealist[] = $k['areaName'];
            $areamaplist[$k['areaName']] = $k['areaName'];
        }
        $jschorg->injectRaw("arealist",json_encode($arealist));
        $jschorg->injectRaw("areamaplist",json_encode($areamaplist));
        // chorgedit is already a string passed into the class
        $jschorg->injectRaw("jsonvaluesphp",$this->chorgedit);
        $jschorg->injectRaw("formmethod",$this->formMethod);
//        $jschorg->injectRaw("jsonvalues",$this->chorgedit);

        $this->customJS_ST = $jschorg;
        return $content;
    }

    public function renderBFD(): string
    {
        $full_name = "{$this->user_edit->givenName} {$this->user_edit->familyName}";
        $tpl = new BfdCmsWrapper(
            $this->user,
            "Editing user: {$full_name}",
            "Editing user: {$full_name}",
            $this->_render(),
            [BfdCmsWrapper::JS_I_CHECK, BfdCmsWrapper::CSS_I_CHECK, BfdCmsWrapper::JS_RED_CONNECT],
            $this->flash
        );
        return $tpl->render();
    }

    public function render(): string
    {
        $tpl = new CmsQaid($this->user);
        $tpl->setTitle('Edit Clinic/Hospital/Org');

        if ($this->flash) {
            $tpl->showFlash($this->flash);
        }
        if ($this->user->inRole(UserRole::ADMIN)) {
            $tpl->toggleAdminLink();
        }
        if ($this->user->inRole(UserRole::PSR)) {
        }
        if ($this->user->inRole(UserRole::ADMIN) ||
            $this->user->inRole(UserRole::PSR)) {
            $tpl->setContent($this->_render());

            $tpl->addCustomScriptBlock($this->customJS_ST);
        }

        return $tpl->render();
    }
}