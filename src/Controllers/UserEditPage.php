<?php

namespace CTAF\Controllers;


use CTAF\Model\UserRole;
use StampTemplateEngine\StampTE;
use CTAF\Controllers\Template\StampTemplate;
use CTAF\Controllers\Template\BfdCmsWrapper;
use CTAF\Model\UserBM;

class UserEditPage implements StampTemplate
{

    const TEMPLATE_FILE = StampTemplate::TEMPLATE_DIR . 'user-edit.html';
    const ENGLISH_ACTION_MSG = "update";
    private $user;
    private $user_edit;
    private $flash;
    private $stamp;

    public function __construct(UserBM $user, UserBM $user_edit, $flash=null)
    {
        $this->flash =$flash;
        $this->user = $user;
        $this->user_edit = $user_edit;
        $this->stamp = new StampTE(file_get_contents(static::TEMPLATE_FILE));
    }


    public function showFlash(string $msg)
    {
        $flash = $this->stamp->get('content.flash');
        if(!empty($msg)){
            $flash->injectRaw('msg', $msg);
        } else {
            $flash->inject('msg', "");
        }
        $this->stamp->add($flash);
    }


    protected function setUser(UserBM $user)
    {
        $this->stamp->inject('USERNAME', $user->userName());
        $this->stamp->inject('USER_FULL_NAME', $user->displayName());
        $this->stamp->inject('USER_TITLE', $user->title);
        if (!$user->inRole(UserRole::PSR)  && !$user->inRole(UserRole::ADMIN)) {
            $this->stamp->inject('role_is_readonly', 'true');
        }
    }


    private function _render(): string
    {
        $content = $this->stamp->get('content');
        $content->inject('button_label', 'Update');
        $content->inject('username', $this->user_edit->username);
        $content->inject('username_readonly', 'readonly');
        $content->inject('first_name', $this->user_edit->givenName);
        $content->inject('last_name', $this->user_edit->familyName);
        $content->inject('middle_name', $this->user_edit->middleName);
        $content->inject('title', $this->user_edit->title);
        $content->inject('email', $this->user_edit->email);
        $content->inject('password1', "");
        $content->inject('password2', "");

        if( $this->user_edit->isActive()){
            $content->inject('active_checked', "checked");
        } else {
            $content->inject('active_checked', " ");
        }
        $content->inject('english_action_msg', static::ENGLISH_ACTION_MSG);
        $content->inject('form_action', \RouteRegistry::USER_EDIT_P);

        if(!empty($this->flash)){
            $this->showFlash($this->flash);
        } else {
            $this->showFlash("");
        }

        $editrolestr = "";

        $roles = $this->user_edit->roles;

        foreach(UserBM::ROLESET as $rolename => $role){
            $templateFrag = "";
            if(in_array($rolename, $roles)){
                $templateFrag = "
                 <div class=\"checkbox\">
                                    <label for=\"role_editor\">
                                        <input type=\"checkbox\"
                                               id=\"role_{$rolename}\"
                                               name=\"roles[]\"
                                               value=\"{$rolename}\"
                                               class=\"margin\"
                                               checked >
                                        {$role}
                                    </label>
                                </div>
                \n";
            } else {
                $templateFrag = "
                 <div class=\"checkbox\">
                                    <label for=\"role_editor\">
                                        <input type=\"checkbox\"
                                               id=\"role_{$rolename}\"
                                               name=\"roles[]\"
                                               value=\"{$rolename}\"
                                               class=\"margin\"
                                               >
                                        {$role}
                                    </label>
                                </div>
                \n";

            }
            $editrolestr .= $templateFrag;
        }

        $content->injectRaw("roleschecks", $editrolestr);
        /*
        if (is_string($roles)) {
            $content->inject("{$roles}_checked", 'checked');
        } else {
            foreach ($roles as $r) {
                $content->inject("{$r}_checked", 'checked');
            }
        }
        */
        return $content;
    }

    public function render(): string
    {
        $full_name = "{$this->user_edit->givenName} {$this->user_edit->familyName}";
        $tpl = new BfdCmsWrapper(
            $this->user,
            "Editing user: {$full_name}",
            "Editing user: {$full_name}",
            $this->_render(),
            [BfdCmsWrapper::JS_I_CHECK, BfdCmsWrapper::CSS_I_CHECK, BfdCmsWrapper::JS_RED_CONNECT],
            $this->flash
        );
        return $tpl->render();
    }
}