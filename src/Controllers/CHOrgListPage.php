<?php

namespace CTAF\Controllers;


use CTAF\Controllers\Template\CmsQaid;
use CTAF\Model\CHOrgBM;
use CTAF\Model\UserRole;
use StampTemplateEngine\StampTE;
use CTAF\Controllers\Template\StampTemplate;
use CTAF\Model\UserBM;

class CHOrgListPage implements StampTemplate
{

    const TEMPLATE_FILE = StampTemplate::TEMPLATE_DIR . 'chorg-list.html';

    private $user;
    private $flash;
    private $stamp;
    private $customJSBlock;

    public function __construct(UserBM $user, $flash=null)
    {
        $this->flash =$flash;
        $this->user = $user;
        $this->stamp = new StampTE(file_get_contents(static::TEMPLATE_FILE));
    }


    public function showFlash(string $msg)
    {
        $flash = $this->stamp->get('content.flash');
        if(!empty($msg)){
            $flash->injectRaw('msg', $msg);
        } else {
            $flash->inject('msg', "");
        }
        $this->stamp->add($flash);
    }

    private function _render(): string
    {
        $content = $this->stamp->get('content');
        $this->customJSBlock = ($this->stamp->get('js_dtchorg'))->getString();

//        $content->inject('username', $this->user_edit->username);

        return $content;
    }

    public function renderBFD(): string
    {
        $full_name = "{$this->user_edit->givenName} {$this->user_edit->familyName}";
        $tpl = new BfdCmsWrapper(
            $this->user,
            "Editing user: {$full_name}",
            "Editing user: {$full_name}",
            $this->_render(),
            [BfdCmsWrapper::JS_I_CHECK, BfdCmsWrapper::CSS_I_CHECK, BfdCmsWrapper::JS_RED_CONNECT],
            $this->flash
        );
        return $tpl->render();
    }

    public function render(): string
    {
        $tpl = new CmsQaid($this->user);
        if ($this->user->inRole(UserRole::ADMIN)) {
            $tpl->setTitle('Area-Clinic/Hospital List');
        } else {
            $tpl->setTitle('Area-Clinic/Hospital List');
        }
        if ($this->flash) {
            $tpl->showFlash($this->flash);
        }
        if ($this->user->inRole(UserRole::ADMIN)) {
            $tpl->toggleAdminLink();
        }
        if ($this->user->inRole(UserRole::PSR)) {
        }
        if ($this->user->inRole(UserRole::ADMIN) ||
            $this->user->inRole(UserRole::PSR)) {
            $tpl->setContent($this->_render());
            $tabulatorcss = $this->stamp->get('tabulator_css');
            $tpl->add($tabulatorcss);
            $tpl->addCustomScriptBlock($this->customJSBlock);
        }

        return $tpl->render();
    }
}