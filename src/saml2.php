<?php

// saml tentants
$saml2 = array();



$saml2_template = array(
    'strict' => true,
    'debug' => false,
    'baseurl' => '',

    'security' => array (
       //'requestedAuthnContext' => false,
       //'nameIdEncrypted' => true,
       //'wantAssertionsEncrypted' => true,
       //'wantAssertionsSigned' => true,
       //'authnRequestsSigned' => true,
       //'logoutRequestSigned' => true,
       //'logoutResponseSigned' => true,
    ),

    'sp' => array(),
    'idp' => array(),

    'userName' => '', //NOT PART OF php-saml
);


// --------------------------------
// SP config
// --------------------------------

$saml2_baseurl = getenv('SAML2_BASEURL') ?: 'http://ctaf.ctlabs.org';

$saml2_template['sp'] = array (
    'entityId' => $saml2_baseurl,
    'assertionConsumerService' => array (
        'url' => $saml2_baseurl.'/saml2/acs',
    ),
    'singleLogoutService' => array (
        'url' => $saml2_baseurl.'/saml2/sls',
    ),
    'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
    'x509cert' => 'MIIDJTCCAg2gAwIBAgIUYbrLdvAivlzhEPKqG9Rm91gzNFkwDQYJKoZIhvcNAQELBQAwIjEgMB4GA1UEAwwXYmZkY21zLnJlZGZsYWdncm91cC5jb20wHhcNMTkwNDIzMTMyOTA5WhcNMjkwNDIwMTMyOTA5WjAiMSAwHgYDVQQDDBdiZmRjbXMucmVkZmxhZ2dyb3VwLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAK2BZcm0As0ox8H55mNRpGwMoppayRAyq3ohNkHz7UdyPoPB2m1ep6nkAxBnhXJFbV/y6ZwJ+sGLai+J9TrsqPoYSSjvQjsxngPehb5nYpciQLVQzui83QRsT2GEiSUfDCqj8PN5e1rvQ6Vpxvhz4IjI/H58CHvI+5WT5/EouPlFXHMVh3ZGnFyi8MLFUxJ+QlLhxo1h9qU7Y1YHSwcl4OcgVqpW889DV4+h8ycSafA6CDiJf2qPajVS9Xq37fPhTtYy8jHd0Q9iUV9lMUexXUUin/b/atD9KfD8Iot+N/oXVYVYZ36nOKHZCNz2VxCRoH+xvx2QFmg7AOliZdCbTLUCAwEAAaNTMFEwHQYDVR0OBBYEFIH2ID/4DyZnpo9tIdQ4FomN9v1XMB8GA1UdIwQYMBaAFIH2ID/4DyZnpo9tIdQ4FomN9v1XMA8GA1UdEwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBABiyEQtztoqDuBM4RxMVeEBEBO5EZNqHrpzUjOgt7zbFqhAupmJXi3v2gsdviwfifoDx/xjHZxHkmBGWlQiQdV2nvllQQONxGDM5Z3ZRw6K6GtGaAvst9I7WrfqsLddpqeos6DAQ56CkpBymdRNH/oc7b5npIWdiHtVqLep+GPVOKhkOEk1CWCLMbdwkwQxZO2r+ByBP/kEle0zVNxG0ygaSevBJe+V+VZvTs7Sc4mavZJnXPO7IdBekOeSxxVv2335kmC6czorz4zQTvBKzvltXHD7R1odci0hrNjY1hKpQK8gt0Is3VTXHP4mKLva49SDjAzzVjtcm+n9Lv7hMwgo=',
    'privateKey' => 'MIIEowIBAAKCAQEArYFlybQCzSjHwfnmY1GkbAyimlrJEDKreiE2QfPtR3I+g8HabV6nqeQDEGeFckVtX/LpnAn6wYtqL4n1Ouyo+hhJKO9COzGeA96FvmdilyJAtVDO6LzdBGxPYYSJJR8MKqPw83l7Wu9DpWnG+HPgiMj8fnwIe8j7lZPn8Si4+UVccxWHdkacXKLwwsVTEn5CUuHGjWH2pTtjVgdLByXg5yBWqlbzz0NXj6HzJxJp8DoIOIl/ao9qNVL1erft8+FO1jLyMd3RD2JRX2UxR7FdRSKf9v9q0P0p8Pwii343+hdVhVhnfqc4odkI3PZXEJGgf7G/HZAWaDsA6WJl0JtMtQIDAQABAoIBAHKwm1E2bbeh4lGl2BYE7LQDsiRoKIpNhC5J7+yuykU1Dn/Xz4QGUrwpnaDlXT/JBuCq2KMtsZTojDt6bSETOSuAjDPD3Dr2tGYIXM4c1v3iRXXVQcB0v4Bh/CMREfr4x/kTv32h6tmUWVnpTv4C0wR4HveZE56Hn04jo+k0qvkSOobwFeELE2UW8ZvKmdnaDFxh9WNSpBg01mdVhFSSjVruzk5DaWFPBtdoK8sfyUfUI7iBhY/NM49trunQajlQHS+rN1YeYK8I+8jA9cLngc8BKo2qQ92BRfgrwNEJlpvTzdZDz4afvoCXFdzM8FSZfd0ks92G+2Hz9U8mLw79nyECgYEA29eGufarcQouxOyYTDlO/OtIpWk/4E/B9aH1vXGgeQ2LyhtYnkOj6Lyh9nMrIJbzepQ++Ah+Ug2WPv3sNT0uRcUK6wEtXkcNjRNRKfq2I9VQ/P7sZMzJL4ATNqpFtNB6NoTJVRimDf/1FbF00hveFk+Y+vCqRs09mBRYmk5x610CgYEAygrefF4u14az1a933v+w07Stm1bk/mDQmBcfOTKa09vNHjrqpW+WGF+YgD/kowCLOpHjTbGgRr818Q7qObqOYe10mId+ZarNzz+xlk1tyoVONvhmgIlt1w8a/dn2LoUnR0GllPgr3Gt6tovdiJA1IrL8bMY9rxMgHRvb9Gf8KTkCgYEAlI81pO9S9WV7K2nylPfZL8LTaawRFUTwfkv/IxDo70S/JUmLfg+ZcQofrMow56n6SOhrEPO0YUB7OoAfIe8BGPbJpPs7rEz5lE7sdyDL+rUJMJ4YIxncDnRPB47S4hhLrhHXd+DbPfSsqwcrOVzJnGwXpeQCPSwuplmBcmz2QKUCgYBeS8JY8zATgRfPpGxi1J03wXzBIkYP/yl7wlbdQCQIuu45rK+09KZty7gGK0vPU8KnXE1lBoDSZA2N6zMgLFSlmciwI5S3aIdTsdt1Vb/nJF5Dw507Ymi0VaV4UtDWF2kklZvtMgJuhm8oYZDMWtmhzz2GmuNRIbD8RPNcV2fV6QKBgEqCqruwIP09O4H1XaLCJtIWoAujavpZIxEpddC6WkV0ZUo5z6GPiq14RePUmt8C854+/eQz1TyXam7MKgzrx8Dn/wQrZtIIOHch1ZJr5OYGQr3XFfDG51GZm23utkFk9w1D1XPGmlZLDIyWYtrubwYRUk2m797aw1TL6pjjZOvv',
    // 'x509certNew' => '',
);




// --------------------------------
// IDP Tenants
// --------------------------------

$saml2['ctaf-bridge'] = $saml2_template;
$saml2['ctaf-bridge']['username'] = 'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress';
$saml2['ctaf-bridge']['sp']['assertionConsumerService']['url'] .= '/ctlab-bridge';
$saml2['ctaf-bridge']['sp']['singleLogoutService']['url'] .= '/ctlab-bridge';
$saml2['ctaf-bridge']['idp'] = array (
    'entityId' => 'https://ctaf-test.ctlabs.org/simplesaml/saml2/idp/metadata.php',
    'singleSignOnService' => array (
        'url' => 'https://ctaf-test.ctlabs.org/simplesaml/saml2/idp/SSOService.php',
    ),
    'singleLogoutService' => array (
        'url' => 'https://ctaf-test.ctlabs.org/simplesaml/saml2/idp/SingleLogoutService.php',
    ),
    'x509cert' => 'MIIFQTCCBCmgAwIBAgIHK4EJSqZI0TANBgkqhkiG9w0BAQsFADCBtDELMAkGA1UEBhMCVVMxEDAOBgNVBAgTB0FyaXpvbmExEzARBgNVBAcTClNjb3R0c2RhbGUxGjAYBgNVBAoTEUdvRGFkZHkuY29tLCBJbmMuMS0wKwYDVQQLEyRodHRwOi8vY2VydHMuZ29kYWRkeS5jb20vcmVwb3NpdG9yeS8xMzAxBgNVBAMTKkdvIERhZGR5IFNlY3VyZSBDZXJ0aWZpY2F0ZSBBdXRob3JpdHkgLSBHMjAeFw0xNDAyMTMwNjQ4MDJaFw0xOTAyMTMwNjQ4MDJaMEUxITAfBgNVBAsTGERvbWFpbiBDb250cm9sIFZhbGlkYXRlZDEgMB4GA1UEAwwXKi5jb21wbGlhbmNlZGVza3RvcC5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDY0YlOESqX/aWC7WEdIWByX4sQaty5Znbr2zWZ76MwdRnee1XZVbES6Vm84brNQH7+t8hoHH1+WwLVj88LFj2cO3ePue6EEwofX2Trjw77hrlCMog3SsGjVMtyesKaMbuMGNecq/gRX6ag93TaifKLTq6sirLHocho4awLJuiVBZysbpIMvjyMovjt5qe3d3R0MB7F+e9qN3/eiOyKzfrXUiv2q9k3+QjediGJFBfTEhAhMGsehYufgtq1yESf+piLO99fF7xafks4P7ZntKJzunbkYxfA9jng1ZrCVmtmODRb0wYg5ekFQqP0sCbqIp7skpI+PprQnjqq22QwKfoLAgMBAAGjggHEMIIBwDAPBgNVHRMBAf8EBTADAQEAMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAOBgNVHQ8BAf8EBAMCBaAwNgYDVR0fBC8wLTAroCmgJ4YlaHR0cDovL2NybC5nb2RhZGR5LmNvbS9nZGlnMnMxLTE1LmNybDBTBgNVHSAETDBKMEgGC2CGSAGG/W0BBxcBMDkwNwYIKwYBBQUHAgEWK2h0dHA6Ly9jZXJ0aWZpY2F0ZXMuZ29kYWRkeS5jb20vcmVwb3NpdG9yeS8wdgYIKwYBBQUHAQEEajBoMCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC5nb2RhZGR5LmNvbS8wQAYIKwYBBQUHMAKGNGh0dHA6Ly9jZXJ0aWZpY2F0ZXMuZ29kYWRkeS5jb20vcmVwb3NpdG9yeS9nZGlnMi5jcnQwHwYDVR0jBBgwFoAUQMK9J47MNIMwojPX+2yz8LQsgM4wOQYDVR0RBDIwMIIXKi5jb21wbGlhbmNlZGVza3RvcC5jb22CFWNvbXBsaWFuY2VkZXNrdG9wLmNvbTAdBgNVHQ4EFgQUYn2l7SvYmb+9fIZbMms9hIsd7aIwDQYJKoZIhvcNAQELBQADggEBAIJ2IzgMgo26k6OF6UaPkb7J9FrMYD2KZ7OJS97+Tc8fwdOrIlO1UIdG+9t9xXtpQHmbQ2wg5SGCliiAytDPZdQvwZc7N8mykmLtK4Z3L1AWUek/Hpy9BPtNQRjoSEsiW5V43AsdV/PWvedLZrbC8l0Us8ipkUZfbZiWns17si9GWK+lX1rNlHajiEcmXMnv9kP0lojOysfnTUkZUFr3XECGQ0Jlms38TgAI/mqJTEoIUgp2dmY3v2m/xTdrwIdwQmwj2/6o/JGFMxMuYONUSGK+TbbGZyC7ZEzNtjkntRIFdYRP+FnFp9n4K/x1XV6hBOBjYJE3r66MNAIX56lDRwc=',
);

$saml2['dummy'] = $saml2_template;
$saml2['dummy']['username'] = 'NameID';
$saml2['dummy']['sp']['assertionConsumerService']['url'] .= '/dummy';
$saml2['dummy']['sp']['singleLogoutService']['url'] .= '/dummy';
$saml2['dummy']['idp'] = array (
    'entityId' => 'https://ssoidp.ctlabs.org/saml2/idp/metadata.php',
    'singleSignOnService' => array (
        'url' => 'https://ssoidp.ctlabs.org/saml2/idp/SSOService.php',
    ),
    'singleLogoutService' => array (
        'url' => 'https://ssoidp.ctlabs.org/saml2/idp/SingleLogoutService.php',
    ),
    'x509cert' => 'MIIFYDCCBEigAwIBAgIHBHIXLPKPsjANBgkqhkiG9w0BAQUFADCByjELMAkGA1UEBhMCVVMxEDAOBgNVBAgTB0FyaXpvbmExEzARBgNVBAcTClNjb3R0c2RhbGUxGjAYBgNVBAoTEUdvRGFkZHkuY29tLCBJbmMuMTMwMQYDVQQLEypodHRwOi8vY2VydGlmaWNhdGVzLmdvZGFkZHkuY29tL3JlcG9zaXRvcnkxMDAuBgNVBAMTJ0dvIERhZGR5IFNlY3VyZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTERMA8GA1UEBRMIMDc5NjkyODcwHhcNMTQwMzIwMDIyODAyWhcNMTYwMzIwMDIyODAyWjBFMSEwHwYDVQQLExhEb21haW4gQ29udHJvbCBWYWxpZGF0ZWQxIDAeBgNVBAMMFyouY29tcGxpYW5jZWRlc2t0b3AuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2NGJThEql/2lgu1hHSFgcl+LEGrcuWZ269s1me+jMHUZ3ntV2VWxEulZvOG6zUB+/rfIaBx9flsC1Y/PCxY9nDt3j7nuhBMKH19k648O+4a5QjKIN0rBo1TLcnrCmjG7jBjXnKv4EV+moPd02onyi06urIqyx6HIaOGsCybolQWcrG6SDL48jKL47eant3d0dDAexfnvajd/3ojsis3611Ir9qvZN/kI3nYhiRQX0xIQITBrHoWLn4LatchEn/qYizvfXxe8Wn5LOD+2Z7Sic7p25GMXwPY54NWawlZrZjg0W9MGIOXpBUKj9LAm6iKe7JKSPj6a0J46qttkMCn6CwIDAQABo4IBzTCCAckwDwYDVR0TAQH/BAUwAwEBADAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwDgYDVR0PAQH/BAQDAgWgMDQGA1UdHwQtMCswKaAnoCWGI2h0dHA6Ly9jcmwuZ29kYWRkeS5jb20vZ2RzMS0xMDYuY3JsMFMGA1UdIARMMEowSAYLYIZIAYb9bQEHFwEwOTA3BggrBgEFBQcCARYraHR0cDovL2NlcnRpZmljYXRlcy5nb2RhZGR5LmNvbS9yZXBvc2l0b3J5LzCBgAYIKwYBBQUHAQEEdDByMCQGCCsGAQUFBzABhhhodHRwOi8vb2NzcC5nb2RhZGR5LmNvbS8wSgYIKwYBBQUHMAKGPmh0dHA6Ly9jZXJ0aWZpY2F0ZXMuZ29kYWRkeS5jb20vcmVwb3NpdG9yeS9nZF9pbnRlcm1lZGlhdGUuY3J0MB8GA1UdIwQYMBaAFP2sYTKTbEXW4u6FX5q653aZaMznMDkGA1UdEQQyMDCCFyouY29tcGxpYW5jZWRlc2t0b3AuY29tghVjb21wbGlhbmNlZGVza3RvcC5jb20wHQYDVR0OBBYEFGJ9pe0r2Jm/vXyGWzJrPYSLHe2iMA0GCSqGSIb3DQEBBQUAA4IBAQBklrmPGn7Kv7VDYEbVpG+igD1jx3jWdMOaOu/uVG761zbYS1fGwUZ7DGSOrKX+tNmkqpxPAPNWTUIiFKY9tmN5aOdg8qxTumoFgmKbTHuWwHe37UjuF9wCZBpSkxbZ6wVF4pX40VlUXhMgruXjUcdhQatgsLY4lO1NdSKOMDwB2t2r9BrIPPaWjsX+wokotpWdZ0rjIF2P9kp1UqnujQBQkVLjpOConvnL0p1mGZGid3UrLrpo8I9ysHx9RTlhwG+MvHw5Or/+U8WcnU+A0u3lA8nO5/IbUNst1j1vC87HT5rzWnvNwK2fXkoBWLhmp33zwm1Lphd6O5RFXrgHGvGB',
);


$idpname = getenv('SAML2_NAME_IDP1') ?: 'idp1';
$saml2[$idpname] = $saml2_template;
$saml2[$idpname]['username'] = getenv('SAML2_USERNAME_IDP1');
$saml2[$idpname]['sp']['assertionConsumerService']['url'] .= '/'.$idpname;
$saml2[$idpname]['sp']['singleLogoutService']['url'] .= '/'.$idpname;
$saml2[$idpname]['idp'] = array (
    'entityId' => getenv('SAML2_ENTITYID_IDP1'),
    'singleSignOnService' => array (
        'url' => getenv('SAML2_SSO_IDP1'),
    ),
    'singleLogoutService' => array (
        'url' => getenv('SAML2_SLO_IDP1'),
    ),
    'x509cert' => getenv('SAML2_CERT_IDP1'),
);


$idpname = getenv('SAML2_NAME_IDP2') ?: 'idp2';
$saml2[$idpname] = $saml2_template;
$saml2[$idpname]['username'] = getenv('SAML2_USERNAME_IDP2') ;
$saml2[$idpname]['sp']['assertionConsumerService']['url'] .= '/'.$idpname;
$saml2[$idpname]['sp']['singleLogoutService']['url'] .= '/'.$idpname;
$saml2[$idpname]['idp'] = array (
    'entityId' => getenv('SAML2_ENTITYID_IDP2'),
    'singleSignOnService' => array (
        'url' => getenv('SAML2_SSO_IDP2'),
    ),
    'singleLogoutService' => array (
        'url' => getenv('SAML2_SLO_IDP2'),
    ),
    'x509cert' => getenv('SAML2_CERT_IDP2'),
);


$idpname = getenv('SAML2_NAME_IDP3') ?: 'idp3';
$saml2[$idpname] = $saml2_template;
$saml2[$idpname]['username'] = getenv('SAML2_USERNAME_IDP3') ;
$saml2[$idpname]['sp']['assertionConsumerService']['url'] .= '/'.$idpname;
$saml2[$idpname]['sp']['singleLogoutService']['url'] .= '/'.$idpname;
$saml2[$idpname]['idp'] = array (
    'entityId' => getenv('SAML2_ENTITYID_IDP3'),
    'singleSignOnService' => array (
        'url' => getenv('SAML2_SSO_IDP3'),
    ),
    'singleLogoutService' => array (
        'url' => getenv('SAML2_SLO_IDP3'),
    ),
    'x509cert' => getenv('SAML2_CERT_IDP3'),
);
