<?php

/**
 * The sole purpose of this class is to get a bird's eye view of all the
 * available routes. Please use this when implementing routes in Slim.
 */
class RouteRegistry
{

    const ROOT = '/';
    const LOGIN = '/login';
    const LOGOUT = '/logout';
    // module_slug
    const LOGIN_EXTERNAL = '/login_external';
    const LOGIN_EXTERNAL_P = '/login_externalP';
    const HOME = '/home';
    const ICON = '/icon';
//    const TESTSCHEMA = '/testschema/{profileId}';
//
//    const ALTCOUNTRIES = '/altcountries';
//    const ALTCOUNTRIESDL = '/altcountries/download';
//    const ALTINDUSTRIES = '/altindustries';
//    const ALTINDUSTRIESDL = '/altindustries/download';
//    const DBSTATS = '/dbstats';
//    const DBSTATSDL = '/dbstats/download';
//    const DBSTATSJSON = '/dbstats/json';
//    const DBSTATSIMPORTERVIOLATIONSJSON = '/dbstats/impvio/json';
//    const DBSTATSCMSVIOLATIONSJSON = '/dbstats/cmsvio/json';

    const ERROR404 = '/error404';
    const ERROR403 = '/error403';
    const ERROR500 = '/error500';

    //
    const P_VIEW = '/p/';
    const P_VIEW__id = '/p/{_id}';
    const P_VIEW_JSON = '/pj/';
    const P_VIEW_JSON__id = '/pj/{_id}';
    const P_VIEW__id__section__sub_section = '/p/{_id}/{_section}/{_sub_section}';
    const SP_VIEW__id__page__panel_subpage__ordinal = '/sp/{_id}/{_page}/{_panel}/{_subpage}/{_ordinal}';
    const SP_VIEW = '/sp/';
    const SP_UPDATE = '/sp/update';
    const P_APPROVE__id = '/p/approve/{_id}';
    const P_UPDATE = '/p/update';

    const P1_VIEW = '/p1/';
    const P1_VIEW__id = '/p1/{_id}';
    const P1_VIEW__id__section__sub_section = '/p1/{_id}/{_section}/{_sub_section}';
    const SP1_VIEW__id__page__panel_subpage__ordinal = '/sp1/{_id}/{_page}/{_panel}/{_subpage}/{_ordinal}';
    const SP1_VIEW = '/sp1/';
    const SP1_UPDATE = '/sp1/update';
    const P1_APPROVE__id = '/p1/approve/{_id}';
    const P1_UPDATE = '/p1/update';

    // profile routes
    const PROFILE = '/profile/';
    const PROFILE_CREATE = '/profile/create';
    const PROFILE_CREATE_SAVE = '/profile/create/save';
    const PROFILE_VIEW__id = '/profile/{_id}';
    const PROFILE_UPDATE = '/profile/update';
    const PROFILE_TYPE_CREATE = '/profile_type/create';
    const PROFILE_TYPE_SAVE = '/profile_type/save';
    const PROFILE_TYPE_UPDATE = '/profile_type/update';
    const PROFILE_TYPE_VIEW = '/profile_type/';
    const PROFILE_TYPE_VIEW__code = '/profile_type/{_code}';
    // reports
    const REPORTS = '/reports/';
    const REPORTS__id = '/reports/{_id}';
    const R__id = '/r/{_id}';
    // user routes
    const USERS = '/users';
    const USER = '/user/';
    const USER_EDIT = '/user/{_id}';
    const USER_EDIT_P = '/user_edit';
    const USER_NEW = '/user_new';
    const USER_NEW_P = '/user_new_p';

//    const COUNTRIES_CREATE = '/country/create';
//    const COUNTRIES_SAVE = '/country/saveCountry';
//    const COUNTRIES_VIEW = '/country/view';
//    const COUNTRIES_VIEW_EDIT = '/country/view_edit/{_id}';
//    const COUNTRIES_EDIT = '/country/save_edit_country';
//    const COUNTRIES_DELETE = '/country/delete_country/{_id}';
//    const SCOPE_CREATE = '/scope/create';
//    const SCOPE_SAVE = '/scope/saveScope';
//    const SCOPE_VIEW = '/scope/view';
//    const SCOPE_VIEW_EDIT = '/scope/view_edit/{_id}';
//    const SCOPE_EDIT = '/scope/save_edit_scope';
//    const SCOPE_DELETE = '/scope/delete_scope/{_id}';
//    const RELATIONSHIPTYPE_CREATE = '/relationshiptype/create';
//    const RELATIONSHIPTYPE_SAVE = '/relationshiptype/saveRelationshiptype';
//    const RELATIONSHIPTYPE_VIEW = '/relationshiptype/view';
//    const RELATIONSHIPTYPE_VIEW_EDIT = '/relationshiptype/view_edit/{_id}';
//    const RELATIONSHIPTYPE_EDIT = '/relationshiptype/save_edit_relationshiptype';
//    const RELATIONSHIPTYPE_DELETE = '/relationshiptype/delete_relationshiptype/{_id}';
//    const BUILDER_CREATE__profile_type = '/builder/create/{_profile_type}';
//    const BUILDER_VIEW__profile_type = '/builder/{_profile_type}';
//    const BUILDER = '/builder/';
//    const BUILDER_SAVE = '/builder/save';
    const CMS_MANAGE = '/cms/manage';


    // CTAF
    const AREAS = '/cms/areas';
    const AREAS_DATA = '/cms/areas/data';
    const AREAS_DATA_P = '/cms/areas/datapost';
    const AREAS_DELETE = '/cms/delete_areas';
//        currently used for area psr assignment dropdown
    const USERS_PSR = '/cms/psrusers/data';

    const CHORG_LIST = '/chorg/list';
    const CHORG_NEW = '/chorg/new';
    const CHORG_EDIT = '/chorg/edit/{_id}';

    const CHORG_LIST_DATA = '/chorg/list/data';
    const CHORG_GET_DATA = '/chorg/{_id}';
    const CHORG_DATA_P = '/chorg/save';
    const CHORG = '/chorg';
    const CHORG_S = '/chorg/';

    const MDS_LIST = '/mds/list';
    const MDS_NEW = '/mds/new';
    const MDS_EDIT= '/mds/edit/{_id}';

    const MDS_LIST_DATA = '/mds/list/data';
    const MDS_GET_DATA = '/mds/{_id}';
    const MDS_DATA_P = '/mds/save';
    const MDS = '/mds';
    const MDSS = '/mds/';

    const TERRITORY = '/cms/territory';
    const MDMASTERLIST = '/cms/mdmasterlist';

    //Report Importer
//    const REPORT_IMPORT = '/reportimporter/report/{_id}';
//    const REPORT_IMPORT_ALL = '/reportimporter/all';
//    // for template L&F dev
    const TEMPLATE_LOGIN = '/tpl/login';
//    const TEMPLATE_CMS = '/tpl/cms';
//    const TEMPLATE_CMS_WORKING = '/tpl/cms-working';
//    const TEMPLATE_CMS_QAID = '/tpl/cms-qaid';
//    const TEMPLATE_CMS_QAID_TEST_RENDER = '/tpl/cms-qaid-test';
//    const TEMPLATE_CMS_QAID_TEST_RENDER_WIDE = '/tpl/cms-qaid-test-wide';
//
//    //Saml2
//    const SAML2_PAGE = '/saml2';
//    const SAML2_INFO = '/saml2/info';
//    const SAML2_TEST_LOGIN = '/saml2/login/{_idp}';
//    const SAML2_TEST_LOGOUT = '/saml2/logout/{_idp}';
//    const SAML2_METADATA = '/saml2/metadata/{_idp}';
//    const SAML2_ACS = '/saml2/acs/{_idp}';
//    const SAML2_SLS = '/saml2/sls/{_idp}';
//
//    //SSO
//    const SSO_LOGIN = '/sso/login/{_idp}';
//    const SSO_LOGOUT = '/sso/logout[/{action}]';
}
