#!/usr/bin/env bash

VERSION=0.0.13-rc-01
ROOT=${PWD}/../../

# git -C ${ROOT} stash
# git -C ${ROOT} fetch --all && git -C ${ROOT} pull
# git -C ${ROOT} stash pop

docker build -t nexus.thinkteam:5001/redcms:${VERSION} -f ${PWD}/Dockerfile ${ROOT}

