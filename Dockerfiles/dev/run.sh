#!/usr/bin/env bash

CONTAINERRUN=/usr/local/bin/docker

if [ "$1" ]; then
  CONTAINERRUN=$1
fi

echo "Running with {$CONTAINERRUN}"

pushd ../..
BRANCH=`git symbolic-ref --short -q HEAD | tr '[:upper:]' '[:lower:]' | sed 's/[^a-zA-Z0-9]/-/g'`
popd

echo "Checking for container on BRANCH ${BRANCH}"
PS_OUT=` $CONTAINERRUN ps -a -f "name=ctaf-${BRANCH}"`
PS_RET=$?
wait

#if [ $PS_RET -ne 0 ]; then
#  echo "Container exists."
#  exit 1
#else
#  echo "Container doesnt exist."
#fi

BRANCH_NAME=ctaf-${BRANCH}

if [[ $PS_OUT = *$BRANCH_NAME* ]] && [ $PS_RET -eq 0 ]; then
    echo "Container already exists. Removing."
    $CONTAINERRUN container rm --force ctaf-${BRANCH}
    RM_RET=$?
    wait
    if [ $RM_RET -ne 0 ]; then
        echo "'docker container rm' returned $RM_RET. Exiting."
        exit 1
    fi
else
  echo "Container doesnt exist."
fi

mkdir -p ../../data/db
mkdir -p ../../data/log/mongodb
mkdir -p ../../data/log/profiles
mkdir -p ../../data/log/tmp
mkdir -p ../../data/log/httpd
echo "" > ../../data/log/httpd/access_log
echo "" > ../../data/log/httpd/error_log
echo "" > ../../data/log/httpd/php_errors.log
echo "" > ../../data/log/mongodb/mongod.log

echo "Attempting to start Docker instance..."
unameOut="$(uname -a)"
#                --network="bridge" \
case "${unameOut}" in
    *Microsoft*)
        machine=LinuxOnWindows
        echo "got $machine"
        echo "PWD: $(pwd) - "
         $CONTAINERRUN -D run \
                --ulimit nofile=4096 \
                -it \
                --name ctaf-${BRANCH} \
                -p 4000:4000 -p 4001:27017 \
                --volume $(wslpath -m $(pwd))/../..:/var/www/ctaf \
                --volume $(wslpath -m $(pwd))/../../data/log/httpd:/var/log/httpd \
                --volume $(wslpath -m $(pwd))/../../data/log/mongodb:/var/log/mongodb \
                --volume $(wslpath -m $(pwd))/../../data/log/tmp:/var/log/tmp \
                -e PHP_IDE_CONFIG="serverName=ctaf-${BRANCH}" \
                -e HOST_UID=1000 \
                -e HOST_GID=1000 \
                ctaf-${BRANCH}
        ;;
    *)
        machine="UNKNOWN:${unameOut}"
        echo "Cannot run on this machine type\n"
        ;;
esac
#cp ./env_devdocker ../../.env
