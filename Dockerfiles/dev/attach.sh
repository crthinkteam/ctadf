#!/usr/bin/env bash

CONTAINERRUN=docker

pushd ../..
BRANCH=`git symbolic-ref --short -q HEAD | tr '[:upper:]' '[:lower:]' | sed 's/[^a-zA-Z0-9]/-/g'`
popd

echo "attaching to: ctaf-${BRANCH}"


 $CONTAINERRUN exec \
  -it \
  ctaf-${BRANCH} \
  /bin/bash

