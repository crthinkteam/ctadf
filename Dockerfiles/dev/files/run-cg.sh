#!/usr/bin/env bash

cd /var/log/profiles

for file in /var/log/profiles/*.cg
do
    if [[ -f $file ]]; then
       rm $file.pdf >> /dev/null
       cg2dot $file | dot -Tpdf -o$file.pdf
       rm $file
       chmod 0777 $file.pdf
       chown root:wheel $file.pdf
    fi
done

