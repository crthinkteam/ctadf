#!/bin/bash

# Make sure we're not confused by old, incompletely-shutdown httpd
# context after restarting the container.  httpd won't start correctly
# if it thinks it is already running.dbs;


# startup Mongodb
rm -rf /var/run/mongodb/*
rm -rf /var/log/mongodb/*
rm -rf /var/log/httpd/*
rm -rf /var/log/tmp/*

/usr/bin/mongod -f /etc/mongod.conf --fork --logpath /var/log/mongodb/mongod.log

# check for valid database
mkdir -p /var/log/tmp

# Wait until mongo logs that it's ready (or timeout after 60s)
COUNTER=0
while !(nc -z localhost 27017) && [[ $COUNTER -lt 60 ]] ; do
    sleep 2
    let COUNTER+=2
    echo "===Waiting for mongo to initialize... ($COUNTER seconds so far)"
done

echo "===Mongo Is Ready, if you now see an error about missing namespace, it is ok, its just how we test for existing database\n"
echo "===Test dump:"
mongodump -d ctafmd -c users -o /var/log/tmp

echo "===Run import"
   pushd /var/www/ctaf/initdata
   /var/www/ctaf/initdata/import.sh
   popd
#if [ $? -ne 0 ] ; then
#fi

#setup remote debugging

#IP=$(/sbin/ip route | awk '/default/ { print $3 }')

#REMOTEHOST="xdebug.remote_host=${IP}";

#grep -Fxq "$REMOTEHOST" /etc/php.d/15-xdebug.ini > /dev/null;
#
#if [ $? -ne 0 ] ; then
#    echo "$REMOTEHOST" >> /etc/php.d/15-xdebug.ini
#fi

#HOSTIP="${IP} dockerhost";
#
#grep -Fxq "$HOSTIP" /etc/hosts > /dev/null;
#
#if [ $? -ne 0 ] ; then
#    echo "$HOSTIP" >> /etc/hosts
#fi
echo "===Back to base dir"
cd /var/www/ctaf

mkdir -p logs
chmod 0777 logs

/usr/bin/php composer.phar global require hirak/prestissimo

if [ "$1" = ci ]; then
    echo "===This was run with CI"
    /usr/bin/php composer.phar install
    /usr/bin/php composer.phar dump-autoload -o
    /usr/sbin/httpd -D FOREGROUND
    exit 0
fi

echo "===Setting env"
    echo "MONGODB_HOST=\"localhost\"" > /var/www/ctaf/.env
    echo "MONGODB_DATABASE_NAME=\"ctafmd\"" >> /var/www/ctaf/.env

echo "===Run composer"
/usr/bin/php composer.phar update
/usr/bin/php composer.phar dump-autoload -o

if [[ -z "$HOST_UID" ]]
then
    echo "===Setting .env and Composer vendor dirs to HOST_UID\n"
    chown -R $HOST_UID:$HOST_UID vendor
    chown -R $HOST_UID:$HOST_UID composer.lock
    chown -R $HOST_UID:$HOST_UID composer.json
    chown -R $HOST_UID:$HOST_UID .env
fi

#cd public
#/usr/bin/php Mustachifier.php
## prime mustache's cache
#/usr/bin/php ApplyCache.php xxx yyy
#cd ..
echo "===Starting apache and bash"
/usr/sbin/httpd -k start
/bin/bash


