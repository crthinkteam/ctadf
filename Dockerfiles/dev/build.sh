#!/bin/bash

CONTAINERRUN=docker

if [ "$1" == "podman" ]; then
    CONTAINERRUN=podman
fi

echo "Running with {$CONTAINERRUN}"

pushd ../..
BRANCH=`git symbolic-ref --short -q HEAD | tr '[:upper:]' '[:lower:]' | sed 's/[^a-zA-Z0-9]/-/g'`
popd

if [ "$1" == "no-cache" ]; then
  echo "Building with no-cache"
  $CONTAINERRUN build --ulimit nofile=4096 --no-cache -t ctaf-${BRANCH} .
  exit $?
else
  echo "Building without no-cache"
  $CONTAINERRUN build --ulimit nofile=4096 -t ctaf-${BRANCH} .
fi
