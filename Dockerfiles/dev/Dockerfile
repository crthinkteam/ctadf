FROM centos:7
MAINTAINER chris.togz@gmail.com

RUN yum clean all
RUN yum update -y

# extra packages for enterprise linux
RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
# remi repository rpms.remirepo.net
RUN yum install -y  http://rpms.remirepo.net/enterprise/remi-release-7.rpm

RUN yum install -y yum-utils
RUN yum-config-manager --enable remi-php73

RUN yum -y update && \
yum -y install  \
gcc \
binutils \
git-core \
httpd \
httpd-devel \
mod_ \
php \
php-devel \
php-pear \
php-cli \
php-xml \
php-tidy \
php-mcrypt \
php-mbstring \
php-bcmath \
php-process \
php-pdo \
php-gd \
php-curl \
php-opcache \
php-zip \
php-pecl-xdebug \
php-pecl-apcu \
php-pecl-mongodb \
php-fileinfo \
openssl-devel \
apachetop \
iputils \
net-tools \
inotify-tools \
iproute \
sysstat \
make \
which \
psmisc \
graphviz \
python-pip \
wget \
ruby \
nc

ADD files/mongodb-org-4.2.repo /etc/yum.repos.d/mongodb-org-4.2.repo
RUN yum install -y mongodb-org-4.2.1 mongodb-org-tools-4.2.1  mongodb-org-mongos-4.2.1 mongodb-org-shell-4.2.1 mongodb-org-server-4.2.1

RUN yum install -y bison byacc ccache cscope ctags elfutils flex \
gcc gcc-c++ gdb glibc-devel indent libtool ltrace oprofile strace valgrind \
&& mkdir -p /var/run/mongodb

RUN yum install -y python-setuptools && \
    cd /root && \
    git clone https://github.com/AlexeyKupershtokh/xdebugtoolkit.git xdebugtoolkit && \
    sed -i 's#> 30#> 130#g' /root/xdebugtoolkit/dot.py && \
    cd xdebugtoolkit && \
    /usr/bin/python setup.py develop

# script to convert cache grind files to pdf
ADD files/run-cg.sh /run-cg.sh

# We open 80 port, the default one for HTTP for Apache server to listen on
EXPOSE 4000
EXPOSE 27017

COPY files/php.ini /etc/php.ini
ADD files/httpd.conf /etc/httpd/conf/httpd.conf
ADD files/mongod.conf /etc/mongod.conf

RUN pecl install channel://pecl.php.net/ahocorasick-0.0.6

ADD php.d/99-date.timezone.conf.ini /etc/php.d/99-date.timezone.conf.ini
ADD php.d/20-ahocorasick.conf.ini /etc/php.d/20-ahocorasick.conf.ini
ADD php.d/15-xdebug.ini /etc/php.d/15-xdebug.ini

# Simple startup script to avoid some issues observed with container restart
ADD files/run-daemons.sh /run-daemons.sh
RUN chmod -v +x /run-daemons.sh
RUN chmod -v +x /run-cg.sh

WORKDIR /var/www/ctaf


ENTRYPOINT ["/run-daemons.sh"]



