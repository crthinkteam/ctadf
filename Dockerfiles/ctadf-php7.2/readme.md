New docker files.

Use the

./build.sh and ./run.sh scripts to build the new docker system

The image creates a /data folder in the project workspace (its in .gitignore) so ts not commited.

This image installs xdebug callbacks and siffs the correct host ipaddress to direct the debug callbacks to the docker host. This means that you can use the chrome xdebug helper to enable/disable the debug session, so log as your ide can listen dfor debug events

I have tested this with phpstorm

The system also supports profiling, activate profiling in the xdebughelper and it will write .cg files with the profile data

it will write these files into the /data/log/profiles folder

use the attach.sh command to attach to the container and run /run-cg.sh to convert the .cg files to pdfs that can be opened in your ide from the profiles folder

The httpd and mongod logs are also directed into the /dat folder, you should be able to open and inspect them in your ide.

---

For non Linux hosts (and for Linux hosts as well), use nobranch-[build|run|attach].sh.