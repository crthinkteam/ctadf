#!/bin/bash

CONTAINERRUN=docker

if [ "$1" == "podman" ]; then
    CONTAINERRUN=podman
fi

echo "Running with {$CONTAINERRUN}"

if [ "$1" == "no-cache" ]; then
    $CONTAINERRUN build --ulimit nofile=4096 --no-cache -t bfdcms-${BRANCH}-php7.2 .
    exit $?
fi

pushd ../..
BRANCH=`git symbolic-ref --short -q HEAD | tr '[:upper:]' '[:lower:]' | sed 's/[^a-zA-Z0-9]/-/g'`
popd

if [ "$1" == "no-cache" ]; then
    $CONTAINERRUN build --ulimit nofile=4096 --no-cache -t bfdcms-${BRANCH}-php7.2 .
    exit $?
fi

$CONTAINERRUN build --ulimit nofile=4096 -t bfdcms-${BRANCH}-php7.2 .
