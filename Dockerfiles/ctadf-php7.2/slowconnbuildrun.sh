#!/bin/bash
# build and run Docker flow slow internet connections

BRANCH='slow-conn'

if [ "$1" == "no-cache" ]; then
    docker build --no-cache -t bfdcms-${BRANCH}-php7.2 .
    exit $?
fi

docker build -t bfdcms-${BRANCH}-php7.2 .

PS_OUT=`docker ps -a -f "name=bfdcms-${BRANCH}-php7.2"`
PS_RET=$?
wait

if [ $PS_RET -ne 0 ]; then
    exit 1
fi

BRANCH_NAME=bfdcms-${BRANCH}-php7.2

if [[ $PS_OUT = *$BRANCH_NAME* ]] && [ $PS_RET -eq 0 ]; then
    echo "Container already exists. Removing."
    docker container rm --force bfdcms-${BRANCH}-php7.2
    RM_RET=$?
    wait
    if [ $RM_RET -ne 0 ]; then
        echo "'docker container rm' returned $RM_RET. Exiting."
        exit 1
    fi
fi


mkdir -p ../../data/db
mkdir -p ../../data/log/mongodb
mkdir -p ../../data/log/profiles
mkdir -p ../../data/log/tmp
mkdir -p ../../data/log/httpd

echo "Attempting to start Docker instance..."
docker run \
        -it \
        --name bfdcms-${BRANCH}-php7.2 \
        -p 4000:80 -p 4001:27017 \
        --volume $(pwd)/../..:/var/www/bfdcms \
        --volume $(pwd)/../../data/log/httpd:/var/log/httpd \
        --volume $(pwd)/../../data/log/mongodb:/var/log/mongodb \
        --volume $(pwd)/../../data/log/tmp:/var/log/tmp \
        --volume $(pwd)/../../data/log/tmp:/var/bfdcms/tmp \
        --volume $(pwd)/../../public/dynamic:/var/bfdcms/public/dynamic \
        bfdcms-${BRANCH}-php7.2
cp ./env_sample ../../.env
