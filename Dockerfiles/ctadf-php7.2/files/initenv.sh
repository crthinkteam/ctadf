#!/bin/bash

cd /var/www/bfgcms
echo '    --> executing `php composer.phar dump-autoload -o`'
php composer.phar dump-autoload -o
cd testdata
echo '    --> executing `./import.sh`'
./import.sh