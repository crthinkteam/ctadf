#!/bin/sh
CURRENTDIR=`pwd`
DATAROOT=$CURRENTDIR/../../data
rm -rf $DATAROOT/db/*
rm -rf $DATAROOT/log/httpd/*
rm -rf $DATAROOT/log/mongodb/*
rm -rf $DATAROOT/log/profiles/*
rm -rf $DATAROOT/log/tmp/*
