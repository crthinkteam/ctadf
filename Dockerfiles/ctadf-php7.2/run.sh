#!/usr/bin/env bash

CONTAINERRUN=docker

if [ "$1" ]; then
  CONTAINERRUN=$1
fi

echo "Running with {$CONTAINERRUN}"

pushd ../..
BRANCH=`git symbolic-ref --short -q HEAD | tr '[:upper:]' '[:lower:]' | sed 's/[^a-zA-Z0-9]/-/g'`
popd

PS_OUT=` $CONTAINERRUN ps -a -f "name=bfdcms-${BRANCH}-php7.2"`
PS_RET=$?
wait

if [ $PS_RET -ne 0 ]; then
    exit 1
fi

BRANCH_NAME=bfdcms-${BRANCH}-php7.2

if [[ $PS_OUT = *$BRANCH_NAME* ]] && [ $PS_RET -eq 0 ]; then
    echo "Container already exists. Removing."
    $CONTAINERRUN container rm --force bfdcms-${BRANCH}-php7.2
    RM_RET=$?
    wait
    if [ $RM_RET -ne 0 ]; then
        echo "'docker container rm' returned $RM_RET. Exiting."
        exit 1
    fi
fi


mkdir -p ../../data/db
mkdir -p ../../data/log/mongodb
mkdir -p ../../data/log/profiles
mkdir -p ../../data/log/tmp
mkdir -p ../../data/log/httpd
echo "" > ../../data/log/httpd/access_log
echo "" > ../../data/log/httpd/error_log
echo "" > ../../data/log/httpd/php_errors.log
echo "" > ../../data/log/mongodb/mongod.log

echo "Attempting to start Docker instance..."
unameOut="$(uname -a)"
case "${unameOut}" in
    *Microsoft*)
        machine=LinuxOnWindows
        echo "got $machine"
         $CONTAINERRUN run \
                --ulimit nofile=4096 \
                -it \
                --name bfdcms-${BRANCH}-php7.2 \
                -p 4000:4000 -p 4001:27017 \
                --volume $(pwd)/../..:/var/www/bfdcms \
                --volume $(pwd)/../../data/log/httpd:/var/log/httpd \
                --volume $(pwd)/../../data/log/mongodb:/var/log/mongodb \
                --volume $(pwd)/../../data/log/tmp:/var/log/tmp \
                --volume $(pwd)/../../data/log/tmp:/var/bfdcms/tmp \
                --volume $(pwd)/../../public/dynamic:/var/bfdcms/public/dynamic \
                -e PHP_IDE_CONFIG="serverName=bfdcms-php7.2" \
                -e HOST_UID=1000 \
                -e HOST_GID=1000 \
                bfdcms-${BRANCH}-php7.2
        ;;
    Linux*)
        machine=Linux
        echo "Docker to run container bfdcms-${BRANCH}-php7.2 on ${machine}"

        if [  -d  /mnt/test-reports ]; then

         echo "";
         echo "mapping in profileReader Test data";
         echo "";

          $CONTAINERRUN run \
                --ulimit nofile=4096 \
                --name bfdcms-${BRANCH}-php7.2 \
                -p 4000:4000 -p 4001:27017 \
                -it \
                --volume $(pwd)/../..:/var/www/bfdcms:Z \
                --volume $(pwd)/../../data/log/httpd:/var/log/httpd:Z \
                --volume $(pwd)/../../data/log/mongodb:/var/log/mongodb:Z \
                --volume $(pwd)/../../data/log/profiles:/var/log/profiles:Z \
                --volume $(pwd)/../../data/db:/var/lib/mongo:Z \
                --volume $(pwd)/../../data/log/tmp:/var/log/tmp:Z \
                --volume $(pwd)/../../public/dynamic:/var/bfdcms/public/dynamic:Z \
                --volume /mnt/test-reports:/mnt/reports:Z \
                  -e PHP_IDE_CONFIG="serverName=bfdcms-php7.2" \
                  -e HOST_UID=$UID \
                  -e HOST_GID=$GID \
                bfdcms-${BRANCH}-php7.2

        else
          $CONTAINERRUN run \
                --ulimit nofile=4096 \
                --name bfdcms-${BRANCH}-php7.2 \
                -p 4000:4000 -p 4001:27017 \
                -it \
                --volume $(pwd)/../..:/var/www/bfdcms:Z \
                --volume $(pwd)/../../data/log/httpd:/var/log/httpd:Z \
                --volume $(pwd)/../../data/log/mongodb:/var/log/mongodb:Z \
                --volume $(pwd)/../../data/log/profiles:/var/log/profiles:Z \
                --volume $(pwd)/../../data/db:/var/lib/mongo:Z \
                --volume $(pwd)/../../data/log/tmp:/var/log/tmp:Z \
                --volume $(pwd)/../../public/dynamic:/var/bfdcms/public/dynamic:Z \
                -e PHP_IDE_CONFIG="serverName=bfdcms-php7.2" \
                -e HOST_UID=$UID \
                -e HOST_GID=$GID \
                bfdcms-${BRANCH}-php7.2

        fi
        ;;
    Darwin*)
        machine=Mac;
        echo "Docker to run container bfdcms-${BRANCH}-php7.2 on ${machine}"
        $CONTAINERRUN run \
                --ulimit nofile=4096 \
                --name bfdcms-${BRANCH}-php7.2 \
                -p 80:4000 -p 4001:27017 \
                -it \
                --volume $(pwd)/../..:/var/www/bfdcms \
                --volume $(pwd)/../../data/log/httpd:/var/log/httpd \
                --volume $(pwd)/../../data/log/mongodb:/var/log/mongodb \
                --volume $(pwd)/../../data/log/profiles:/var/log/profiles \
                --volume $(pwd)/../../data/db:/var/lib/mongo \
                --volume $(pwd)/../../data/log/tmp:/var/log/tmp \
                --volume $(pwd)/../../public/dynamic:/var/bfdcms/public/dynamic \
                  -e PHP_IDE_CONFIG="serverName=bfdcms-php7.2" \
                  -e HOST_UID=$UID \
                  -e HOST_GID=$GID \
                bfdcms-${BRANCH}-php7.2
        ;;
    *)
        machine="UNKNOWN:${unameOut}"
        echo "Cannot run on this machine type\n"
        ;;
esac
cp ./env_devdocker ../../.env
sudo chown