$(function () {
    'use strict';

    // Enable check and uncheck all functionality
    $('.checkbox-toggle').click(function () {
        if ($(this).children().first().hasClass('fa-check-square-o')) {
            // Uncheck all checkboxes
            $('.mailbox-messages input:checkbox').iCheck('uncheck');
            $('.fa.fa-check-square-o').removeClass('fa-check-square-o').addClass('fa-square-o');
        } else {
            // Check all checkboxes
            $('.mailbox-messages input:checkbox').iCheck('check');
            $('.fa.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square-o');
        }
    });

    // Handle starring for glyphicon and font awesome
    $('.mailbox-star').click(function (e) {
        e.preventDefault();

        // Detect type
        var $this = $(this).find('a > i');
        var glyph = $this.hasClass('glyphicon');
        var fa = $this.hasClass('fa');

        // Switch states
        if (glyph) {
            $this.toggleClass('glyphicon-star');
            $this.toggleClass('glyphicon-star-empty');
        }

        if (fa) {
            $this.toggleClass('fa-star');
            $this.toggleClass('fa-star-o');
        }
    });
});