$(function () {
    'use strict';
// summernote config global
    var keepOnlyTagslist = ['<br>', '<ul>', '<li>', '<b>', '<i>', '<p>'];
    /**
     * Search
     */
    if ($('#search').length > 0) {
        $('#search-string').on('keydown', function (e) {
            var key = e.keyCode ? e.keyCode : e.which;
            if (key === 13) {
                $('#search').trigger('click');
            }
        });

        $('#search').on('click', function () {
            /**
             * For DEMO purpose only
             *
             * TODO:
             * Change this with actual searching instructions
             */
            var searchString = $.trim($('#search-string').val().toLowerCase());
            if (searchString === 'bank') {
                $('#results').show();
                $('#results-empty').hide();
            } else if (searchString === '') {
                $('#results').hide();
                $('#results-empty').hide();
            } else {
                $('#results').hide();
                $('#results-empty').show();
            }
        });
    }

    /**
     * Results table
     */
    if ($('#results-table-empty, #results-table').length > 0) {
        /**
         * For DEMO purpose only
         *
         * TODO:
         * Remove $('#results-table-empty').DataTable({...}) definition later
         */
        $('#results-table-empty').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'order': [],
            'info': false,
            'language': {
                'emptyTable': '<div class="mb-30"><h3><i class="fa fa-info-circle margin"></i>No Match Found</h3><p>We couldn\'t find the profile you were looking for.</p><button id="create-new-profile" type="button" class="btn bg-black btn-default">Create New Profile</button></div>'
            },
            'fnDrawCallback': function () {
                $('#results-table-empty_paginate').hide();
            }
        });

        $('#results-table').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'order': [],
            'info': false,
            'language': {
                'emptyTable': '<div class="mb-30"><h3><i class="fa fa-info-circle margin"></i>No Match Found</h3><p>We couldn\'t find the profile you were looking for.</p><button id="create-new-profile" type="button" class="btn bg-black btn-default">Create New Profile</button></div>'
            },
            'fnDrawCallback': function () {
                if ($('#results-table_paginate>ul>li.paginate_button').length < 3) {
                    $('#results-table_paginate').hide();
                }
            }
        });

        $('#create-new-profile').on('click', function () {
            /**
             * For DEMO purpost only
             *
             * TODO:
             * Change the following line with appropriate codes.
             */
            alert('You clicked on "Create New Profile" button!');
        });
    }

    var unsaved = false;
    $('.wysiwyg').summernote({
        toolbar: [
            ['style', ['bold', 'italic', 'ul']],
            ['misc', ['help']]
        ],
        height: 200,
        callbacks: {
            onChange: function (x, y) {
                unsaved = true;
            }
        },
        cleaner:{
            action: 'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
            newline: '<br>', // Summernote's default is to use '<p><br></p>'
            notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
            icon: '<i class="note-icon">[Your Button]</i>',
            keepHtml: true, // Remove all Html formats
            keepOnlyTags: keepOnlyTagslist, // If keepHtml is true, remove all tags except these
            keepClasses: false, // Remove Classes
            badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
            badAttributes: ['style', 'start'], // Remove attributes from remaining tags
            limitChars: false, // 0/false|# 0/false disables option
            limitStop: false // true/false
        }
    });

    $('input:checkbox, input:radio').iCheck({
        checkboxClass: 'icheckbox_square',
        radioClass: 'iradio_square'
    });

    $('input:radio').on('ifClicked', function () {
        $(this).iCheck('uncheck');
    });

    $('.select2').select2({allowClear: true, placeholder: true});
    $('.select2panel').select2({allowClear: true, placeholder: true});
    $('.select2source').select2({allowClear: true, placeholder: true});

    if ($('#username').attr('role-is-readonly') == 'true') {
        $('input').prop('readonly', true);
        $('.wysiwyg').each(function (idx) {
            $(this).summernote('disable');
        });
        $('input:checkbox, input:radio').each(function (idx) {
            $(this).iCheck('disable');
        });
        $('.select2').each(function (idx) {
            $(this).prop('disabled', true);
        });
        $('.select2panel').each(function (idx) {
            $(this).prop('disabled', true);
        });
        $('.select2source').each(function (idx) {
            $(this).prop('disabled', true);
        });
        $('#approve-profile-edit').hide();
        $('#profile_editor_submit').hide();
        $('#print_report').hide();
        return;
    }


    $('.has-source-anchor').on('click', 'button.add-source', function () {
        $(this).closest('tr').find('select').select2('destroy');
        var cloned = $(this).closest('tr').clone();
        cloned.find('input[type=text]').val('');
        cloned.find('select').prop('selectedIndex', 0);
        $(this).removeClass('add-source').addClass('delete-source');
        $(this).find('span').removeClass('glyphicon-plus').addClass('glyphicon-trash');
        $(this).closest('tbody').append(cloned);
        $(this).closest('table').find('select').select2({allowClear: true, placeholder: true});
    });

    $('.has-source-anchor').on('click', 'button.delete-source', function () {
        $(this).closest('tr').remove();
    });

    $('.hidden-photo-zap').on('click', function () {
        $(this).siblings('.hidden-photo').remove();
        $(this).siblings('input[type=hidden]').remove();
        $(this).closest('.form-group').find('table.has-source button.delete-source').closest('tr').remove();
        $(this).closest('.form-group').find('table.has-source select').val('').trigger('change');
        $(this).closest('.form-group').find('table.has-source input[type=text]').val('');
    });

    $('.bfd-panel').on('click', 'button.delete-element-in-panel', function () {
        $(this).closest('.form-group').remove();
    });
    $('.bfd-panel').on('click', 'button.add-element-in-panel', function () {
        $(this).closest('.cloneable').find('select').select2('destroy');
        $(this).closest('.cloneable').find('textarea').each(function (idx, sm) {
            $(sm).summernote('destroy');
        });
        var cloned = $(this).closest('.form-group').clone();
        cloned.find('input[type=text], textarea').val('');
        cloned.find('button.delete-source').closest('tr').remove();
        cloned.find('table.has-source button.delete-source').remove();
        $(this).removeClass('add-element-in-panel').addClass('delete-element-in-panel');
        $(this).find('span').removeClass('glyphicon-plus').addClass('glyphicon-trash');
        $(this).closest('.cloneable').append(cloned);
        $(this).closest('.cloneable').find('textarea').each(function (idx, sm) {
            $(sm).summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'ul']],
                    ['misc', ['help']]
                ],
                height: 200,
                callbacks: {
                    onChange: function (x, y) {
                        unsaved = true;
                    }
                },
                cleaner:{
                    action: 'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                    newline: '<br>', // Summernote's default is to use '<p><br></p>'
                    notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
                    icon: '<i class="note-icon">[Your Button]</i>',
                    keepHtml: true, // Remove all Html formats
                    keepOnlyTags: keepOnlyTagslist, // If keepHtml is true, remove all tags except these
                    keepClasses: false, // Remove Classes
                    badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
                    badAttributes: ['style', 'start'], // Remove attributes from remaining tags
                    limitChars: false, // 0/false|# 0/false disables option
                    limitStop: false // true/false
                }
            });
        });
        $(this).closest('.cloneable').find('select').select2({allowClear: true, placeholder: true});
    });

    $('.bfd-panel').on('click', 'button.add-panel', function () {
        $(this).closest('.bfd-panel').find('select').select2('destroy');
        $(this).closest('.bfd-panel').find('input:checkbox, input:radio').iCheck('destroy');

        $(this).closest('.bfd-panel').find('textarea').each(function (idx, sm) {
            $(sm).summernote('destroy');
        });

        var cloned = $(this).closest('.panel-body').clone();
        cloned.find('button.delete-source').closest('tr').remove();
        cloned.find('input[type=text], textarea').val('');
        cloned.find('input[type=file], textarea').val('');
        cloned.find('select').prop('selectedIndex', 0);
        cloned.find('input:radio, input:checkbox').prop('checked', false);
        cloned.find('input:radio, input:checkbox').each(function (idx, elem) {
            var name = $(elem).prop('name');
            $(elem).prop('name', name + '_xyx_');
        });
        cloned.find('table.has-source button.delete-source').remove();
        cloned.find('div.cloneable div.form-group button.delete-element-in-panel').each(function () {
            $(this).closest('.form-group').remove();
        });
        $(this).next().remove();
        $(this).removeClass('add-panel').addClass('delete-panel');
        $(this).find('span').removeClass('glyphicon-plus').addClass('glyphicon-trash');
        $(this).closest('.bfd-panel').append(cloned);
        $(this).closest('.bfd-panel').find('select').select2({allowClear: true, placeholder: true});
        $(this).closest('.bfd-panel').find('input:checkbox, input:radio').iCheck({
            checkboxClass: 'icheckbox_square',
            radioClass: 'iradio_square'
        });
        $(this).closest('.bfd-panel').find('input:radio').on('ifClicked', function () {
            $(this).iCheck('uncheck');
        });

        $(this).closest('.bfd-panel').find('textarea').each(function (idx, sm) {
            $(sm).summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'ul']],
                    ['misc', ['help']]
                ],
                height: 200,
                callbacks: {
                    onChange: function (x, y) {
                        unsaved = true;
                    }
                },
                cleaner:{
                    action: 'both', // both|button|paste 'button' only cleans via toolbar button, 'paste' only clean when pasting content, both does both options.
                    newline: '<br>', // Summernote's default is to use '<p><br></p>'
                    notStyle: 'position:absolute;top:0;left:0;right:0', // Position of Notification
                    icon: '<i class="note-icon">[Your Button]</i>',
                    keepHtml: true, // Remove all Html formats
                    keepOnlyTags: keepOnlyTagslist, // If keepHtml is true, remove all tags except these
                    keepClasses: false, // Remove Classes
                    badTags: ['style', 'script', 'applet', 'embed', 'noframes', 'noscript', 'html'], // Remove full tags with contents
                    badAttributes: ['style', 'start'], // Remove attributes from remaining tags
                    limitChars: false, // 0/false|# 0/false disables option
                    limitStop: false // true/false
                }
            });
        });
    });

    $('.panel').on('click', 'button.delete-panel', function () {
        $(this).closest('.panel-body').remove();
    });


    // renumber source input elements on submit
    $('#profile_editor_submit').on('click', function () {
        $('.bfd-panel').each(function (panelctr, panel) {
            $(panel).find('.panel-body').each(function (ipanelbody, element) {
                $(element).find('.bfd-radio-group input[type=radio]').each(function (idx, cb) {
                    $(cb).each(function (idx, cbc) {
                        var old_name = $(cbc).attr('name');
                        old_name = old_name.replace(/__mark_[0-9]+__/, ipanelbody);
                        var new_name = old_name.replace(/_xyx_/g, '');
                        $(cbc).attr('name', new_name);
                    })
                });
                $(element).find('.checkbox-group input[type=checkbox]').each(function (idx, cb) {
                    $(cb).each(function (idx, cbc) {
                        var old_name = $(cbc).attr('name');
                        old_name = old_name.replace(/__mark_[0-9]+__/, ipanelbody);
                        var new_name = old_name.replace(/_xyx_/g, '');
                        $(cbc).attr('name', new_name);
                    })
                });
                $(element).find('table.has-source select').each(function (idx, select) {
                    var old_name = $(select).attr('name');
                    var new_name = old_name.replace('_renumber_', ipanelbody);
                    $(select).attr('name', new_name);
                });
                $(element).find('table.has-source input[type=text]').each(function (idx, input) {
                    var old_name = $(input).attr('name');
                    var new_name = old_name.replace('_renumber_', ipanelbody);
                    $(input).attr('name', new_name);
                });
                $(element).find('.cloneable').each(function (idx, cln) {
                    $(cln).find('.cloneable_textarea textarea.form-control').each(function (idx_ta, txta) {
                        var old_name = $(txta).attr('name');
                        var new_name = old_name.replace('__panelordinal__', ipanelbody);
                        $(txta).attr('name', new_name);
                        $(txta).parents('.cloneable_textarea').find('table.has-source select')
                            .each(function (idx_sel, sel) {
                                var old_name = $(sel).attr('name');
                                var new_name = old_name.replace('__panelordinal__', ipanelbody);
                                var newer_name = new_name.replace('__cloneordinal__', idx_ta);
                                $(sel).attr('name', newer_name);
                            });
                        $(txta).parents('.cloneable_textarea').find('table.has-source input[type=text]')
                            .each(function (idx_inp, txt) {
                                var old_name = $(txt).attr('name');
                                var new_name = old_name.replace('__panelordinal__', ipanelbody);
                                var newer_name = new_name.replace('__cloneordinal__', idx_ta);
                                $(txt).attr('name', newer_name);
                            });
                    });
                });
            });
        });
        unsaved = false;
    });
    $(":input").change(function () {
        unsaved = true;
    });
    $(".box-ctaf").on('ifToggled', 'input[type=radio], input[type=checkbox]', function () {
        unsaved = true;
    });

    function unloadPage() {
        if (unsaved) {
            return "You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?";
        }
    }

    window.onbeforeunload = unloadPage;

    // bad hack but we need this quick
    if ($('#txta-riskAndViews').length) {
        $('#txta-riskAndViews').summernote('destroy');
        $('#txta-riskAndViews').siblings('label').after('<span id="maxContentPost"></span>');
        $('#txta-riskAndViews').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'ul']],
                ['misc', ['help']]
            ],
            height: 200,
            placeholder: 'Maximum of 242 characters.',
            callbacks: {
                onChange: function (x, y) {
                    unsaved = true;
                },
                onKeydown: function (e) {
                    var t = e.currentTarget.innerText;
                    if (t.trim().length >= 242) {
                        //delete keys, arrow keys, copy, cut
                        if (e.keyCode != 8
                            && !(e.keyCode >= 37 && e.keyCode <= 40)
                            && e.keyCode != 46
                            && !(e.keyCode == 88 && e.ctrlKey)
                            && !(e.keyCode == 67 && e.ctrlKey))
                            e.preventDefault();
                    }
                },
                onKeyup: function (e) {
                    var t = e.currentTarget.innerText;
                    var riskViewLimitMsg = '';
                    if (t.length  > 242) {
                        riskViewLimitMsg = ' Limit exceeded. Please revise.';
                    }
                    $('#maxContentPost').text(' ( ' + (242 - t.trim().length) + ' left )' + riskViewLimitMsg);
                },
                onPaste: function (e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData)
                        .getData('Text');
                    e.preventDefault();
                    var maxPaste = bufferText.length;
                    if (t.length + bufferText.length > 242) {
                        maxPaste = 242 - t.length;
                    }
                    if (maxPaste > 0) {
                        // document.execCommand('insertText', false, bufferText.substring(0, maxPaste));
                    }
                    $('#maxContentPost').text(' ( ' + (242 - t.trim().length) + ' left )');
                }
            }
        });
    }
});


