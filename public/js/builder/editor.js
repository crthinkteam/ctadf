'use strict';

// Custom properties for Survey
Survey.JsonObject.metaData.addProperty('survey', {
    'name': 'section',
    'default': 'Main Section',
    'choices': ['Main Section', 'Sub-Section', 'Section Child']
});
Survey.JsonObject.metaData.addProperty('survey', {
    'name': 'controlType',
    'default': 'Tab',
    'choices': ['Content', 'Page', 'Step', 'Tab']
});
Survey.JsonObject.metaData.addProperty('survey', {
    'name': 'version',
    'default': '1.0'
});

// Custom properties for Page
Survey.JsonObject.metaData.addProperty('page', {
    'name': 'section',
    'default': '',
    'choices': ['', 'Main Section', 'Sub-Section', 'Section Child']
});
Survey.JsonObject.metaData.addProperty('page', {
    'name': 'controlType',
    'choices': ['', 'Content', 'Page', 'Step', 'Tab']
});

Survey.JsonObject.metaData.addProperty('page', {
    'name': 'slug',
});

Survey.JsonObject.metaData.addProperty('page', {
    'name': 'subPage:boolean',
});

Survey.JsonObject.metaData.addProperty('page', {
    'name': 'superPageSlug',
});

Survey.JsonObject.metaData.addProperty('panel', 'valueName');
Survey.JsonObject.metaData.addProperty('panel', 'subPageEditor');

// Custom properties for Questionbase
Survey.JsonObject.metaData.addProperty('questionbase', 'dataParam');
Survey.JsonObject.metaData.addProperty('questionbase', 'hasSource:boolean');
Survey.JsonObject.metaData.addProperty('questionbase', 'photoUpload:boolean');
Survey.JsonObject.metaData.addProperty('questionbase', 'cloneable:boolean');
Survey.JsonObject.metaData.addProperty('questionbase', 'placeHolderText');
Survey.JsonObject.metaData.addProperty('questionbase', 'choicesFromModel');
Survey.JsonObject.metaData.addProperty('questionbase', 'dataFromModel');
Survey.JsonObject.metaData.addProperty('questionbase', {
    'name': 'style',
    'default': 'Standard',
    'choices': ['Standard', 'WYSIWYG']
});

delete Survey.matrixDropdownColumnTypes['boolean'];
delete Survey.matrixDropdownColumnTypes['text'];
delete Survey.matrixDropdownColumnTypes['expression'];
delete Survey.matrixDropdownColumnTypes['checkbox'];
delete Survey.matrixDropdownColumnTypes['radiogroup'];

// Editor options
var editorOptions = {
    // Show the embeded survey tab. It is hidden by default
    showEmbededSurveyTab: false,
    // Hide the test survey tab. It is shown by default
    showTestSurveyTab: false,
    // Hide the JSON text editor tab. It is shown by default
    showJSONEditorTab: false,
    // Show the "Options" button menu. It is hidden by default
    showOptions: false,
    // Show the current status on the Toolbar: Saving/Saved/Error
    showState: true,
    // Show only specific question types in Toolbox
    questionTypes: ['checkbox', 'dropdown', 'panel', 'radiogroup', 'text']
};

// Pass the editorOptions into the constructor. It is an optional parameter.
var editor = new SurveyEditor.SurveyEditor(null, editorOptions);

// Define saving function
editor.saveSurveyFunc = function () {
    var editorData = editor.text;

    // Send updated json in your storage
    $.ajax({
        'cache': false,
        'contentType': 'application/json',
        'dataType': 'json',
        'data': editorData,
        'method': 'post',
        'url': '/builder/save'
    }).done(function (resp) {
        console.log(JSON.stringify(resp.data));
        alert('Response status: ' + resp.status);
    });
}

// Custom toolbox items (Multiline Text and Tabular Data)
editor.toolbox.addItems([
    {
        'name': 'multiline',
        'iconName': 'icon-editor',
        'title': 'Multiline Text',
        'json': {'type': 'comment'}
    },
    {
        'name': 'tabulardata',
        'iconName': 'icon-datepicker',
        'title': 'Tabular Data',
        'json': {
            'type': 'matrixdynamic',
            'addRowText': 'Add Row',
            'cellType': 'text',
            'columns': [
                {
                    'name': 'col1'
                },
                {
                    'name': 'col2'
                },
            ],
            'rowCount': 1
        }
    }
]);

// Rearrange objects
editor.toolbox.orderedQuestions = [
    'panel', 'text', 'checkbox', 'radiogroup', 'dropdown', 'multiline', 'tabulardata'
];

// Show usable properties only
var allowedProps = {
    'survey': [
        'controlType', 'section', 'title', 'version'
    ],
    'page': [
        'controlType', 'description', 'name', 'section', 'slug', 'subPage', 'superPageSlug'
    ],
    'panel': [
        'controlType', 'page', 'section', 'title', 'valueName', 'subPageEditor'
    ],
    'text': [
        'dataParam', 'inputType', 'isRequired', 'page', 'placeHolderText', 'readOnly',
        'title', 'valueName', 'hasSource', 'photoUpload'
    ],
    'checkbox': [
        'choices', 'choicesFromModel', 'choicesOrder', 'colCount', 'dataParam',
        'isRequired', 'page', 'readOnly', 'title', 'valueName', 'hasSource'
    ],
    'radiogroup': [
        'choices', 'choicesFromModel', 'choicesOrder', 'colCount', 'dataParam',
        'isRequired', 'page', 'readOnly', 'title', 'valueName', 'hasSource'
    ],
    'dropdown': [
        'choices', 'choicesFromModel', 'choicesOrder', 'dataParam', 'isRequired',
        'page', 'placeHolderText', 'readOnly', 'title', 'valueName', 'hasSource'
    ],
    // Multiline Text
    'comment': [
        'cols', 'dataParam', 'isRequired', 'page', 'placeHolder', 'readOnly', 'rows',
        'style', 'title', 'valueName', 'hasSource', 'cloneable'
    ],
    // Tabular data
    'matrixdynamic': [
        'dataFromModel', 'columns', 'page', 'title', 'valueName'
    ]
};
editor.onCanShowProperty.add(function (sender, options) {
    var propName = options.property.name;
    var objType = (typeof options.obj.getType !== 'undefined') ? options.obj.getType() : null;
    var canShow = false, i;

    for (i = 0; i < allowedProps[objType].length; i++) {
        if (propName === allowedProps[objType][i]) {
            canShow = true;
            break;
        }
    }

    options.canShow = canShow;
});

// Remove menu items "Hide/Show Title" and "Add To ToolBox"
editor.onDefineElementMenuItems.add(function (editor, options) {
    for (var i = 0; i < options.items.length; ++i) {
        if (options.items[i].name === 'showtitle' || options.items[i].name === 'addtotoolbox') {
            options.items.splice(i, 1);
        }
    }
});
editor.render('surveyEditorContainer');
