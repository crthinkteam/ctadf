$(function () {
    'use strict';

    /**
     * Tooltip
     */
    if ($('[data-toggle="tooltip"]').length > 0 && typeof $.fn.tooltip !== 'undefined') {
        $('[data-toggle="tooltip"]').tooltip();
    }

    /**
     * jQuery Validatation
     */
    if (typeof $.fn.validate !== 'undefined') {
        jQuery.validator.setDefaults({
            'highlight': function (element) {
                $(element)
                    .closest('.form-group')
                    .addClass('has-error');

                // Revert <label> color back to black (#333)
                if ($(element).is('input:checkbox')) {
                    $('.checkbox > label').css('color', '#333');
                }

                // TODO: Replace this with Toastr plugin
                $('#alert-danger').removeClass('hidden');
            },
            'unhighlight': function (element) {
                $(element)
                    .closest('.form-group')
                    .removeClass('has-error');
            },
            'errorPlacement': function (error, element) {
                // console.log($(element).parent().parent().parent().prop('nodeName'));
                if ($(element).parent().parent().parent('.checkbox').length) {
                    $('.checkbox-group').append(error);       // checkbox-group iCheck
                } else if ($(element).parent().parent('.checkbox').length) {
                    $('.checkbox-group').append(error);       // checkbox-group input:checkbox
                } else if ($(element).parent().parent().parent('.radio').length) {
                    $('.radio-group').append(error);          // radio-group iCheck
                } else if ($(element).parent().parent('.radio').length) {
                    $('.radio-group').append(error);          // radio-group input:radio
                } else if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());      // input-group radio/checkbox
                } else if (element.hasClass('select2-hidden-accessible')) {
                    error.insertAfter(element.next('span'));  // select2
                } else {
                    error.insertAfter(element);               // default
                }
            }
        });
    }

    /**
     * Apply Select2 plugin on <select>
     */
    if ($('select').length > 0 && typeof $.fn.select2 !== 'undefined') {
        $('select').select2({
            width: '100%'
        }).on('change', function () {
            if (typeof $.fn.validate !== 'undefined') {
                $(this).valid();
            }
        });

        /**
         * Reset Select2 value
         */
        if ($('button:reset').length > 0) {
            $('button:reset').on('click', function () {
                $('select').val(null).trigger('change');
            });
        }
    }

    /**
     * Enable iCheck plugin for checkboxes and radio buttons
     */
    if (($('input:checkbox').length > 0 || $('input:radio').length > 0) && typeof $.fn.iCheck !== 'undefined') {
        $('input:checkbox, input:radio').iCheck({
            checkboxClass: 'icheckbox_square',
            radioClass: 'iradio_square'
        }).on('ifChecked', function () {
            if (typeof $.fn.validate !== 'undefined') {
                $(this).valid();
            }
        });
    }

    /**
     * WYSIWYG
     */
    if ($('.wysiwyg').length > 0) {
        $('.wysiwyg').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'ul']],
                ['misc', ['help']]
            ],
            height: 200
        });
    }

    $('#search-results-table').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': false,
        'dom': 'iptp'
    });
});
