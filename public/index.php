<?php

require __DIR__ . '/version.php';

use CTAF\Controllers\ErrorPage;

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';



// Adding dotenv support
try {
    $dotenv = (new \Dotenv\Dotenv(__DIR__ . '/../'))->load();
} catch (\Dotenv\Exception\InvalidPathException $ex) {
    die("check .env");
}

$session_collection = (new \CTAF\DAO\PhpSessionDAO())->collection();
$session_handler = new \Altmetric\MongoSessionHandler($session_collection);

session_set_save_handler($session_handler);
session_set_cookie_params(0, '/', null, false, true);
session_name('ctaf');
session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);
$container = $app->getContainer();

unset($container['notFoundHandler']);
$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        $user = $c->session->get(SessionKeys::USER_BM);
        if ($user === null) {
            $user = new \CTAF\Model\UserBM(true);
        }
        $page = new ErrorPage($user, 404);
        $flash = $c->flash->getFirstMessage(RouteRegistry::ERROR404);
        if ($flash) {
            $page->flash = $flash;
        }
        return $response->write($page->render())->withStatus(404);
    };
};

unset($container['notAllowedHandler']);
$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        $user = $c->session->get(SessionKeys::USER_BM);
        $page = new ErrorPage($user, 403);
        $flash = $c->flash->getFirstMessage(RouteRegistry::ERROR403);
        if ($flash) {
            $page->flash = $flash;
        }
        return $response->write($page->render())->withStatus(403);
    };
};


// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Set up dependency for mongodb. This way dev edits local config only.
require __DIR__ . '/../src/mongodb.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

require __DIR__ . '/../src/RouteRegistry.php';
require __DIR__ . '/../src/SessionKeys.php';
// Register routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
