<?php

$fload = fopen(__DIR__ . '/bfd_cms_users_load.csv', 'w');
$fplain = fopen(__DIR__ . '/bfd_cms_users_plain.csv', 'w');
$file = fopen(__DIR__ . '/userpass.csv', 'r');
fputs($fload, '"username","password","first_name","last_name"'. "\n");
$json = [];
while (!feof($file)) {
    $l = fgetcsv($file);
    if (trim($l[0]) == '') {
        break;
    }
    $username = $l[1];
    $plain = substr(md5($l[0]), 0, 8);
    $password = md5($plain);
    $ntoks = explode(' ', $l[0]);
    $first_name = $ntoks[0];
    $last_name = substr($l[0], strlen($first_name) + 1, strlen($l[0]));
    fputs($fload, "{$username},{$password},{$first_name},{$last_name}\n");
    fputs($fplain, "{$username},{$plain}\n");
    $json[] = [
        'userName' => $username,
        'password' => $password,
        'givenName' => $first_name,
        'familyName' => $last_name,
    ];
}
fclose($file);
fclose($fload);
fclose($fplain);
file_put_contents(__DIR__ . '/bfd_cms_users_load.json', json_encode($json, JSON_PRETTY_PRINT));
