#!/bin/bash

if [ -z "$WORKSPACEROOT" ]; then
WORKSPACEROOT=/var/www/ctaf
fi

cd $WORKSPACEROOT/initdata
#MONGODB_HOST=$(echo $MONGODB_HOST | tr -d '\r')

source ../.env

echo "===Starting mongo ping"

while true
do
    mongo --host $MONGODB_HOST 10pingdb.js
    if (( $? == 0 )); then
        break
    fi
    sleep 1
done

echo "===Current directory is:" `pwd`

#for i in users areas chorg contactinfotype products specialties
for i in users areas chorg lookup
do
    mongoimport --jsonArray --host $MONGODB_HOST --db $MONGODB_DATABASE_NAME --collection $i --drop --file $i.json
    wait;
done

#mongo $MONGODB_HOST/$MONGODB_DATABASE_NAME 00profiles-fulltext-search.js
#mongo $MONGODB_HOST/$MONGODB_DATABASE_NAME 01unique-indices.js

echo "===Import done."