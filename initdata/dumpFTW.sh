#!/bin/bash

if [ -z "$WORKSPACEROOT" ]; then
WORKSPACEROOT=/var/www/ctaf
fi

cd $WORKSPACEROOT/initdata
#MONGODB_HOST=$(echo $MONGODB_HOST | tr -d '\r')

source ../.env

echo "===Starting mongo ping"

while true
do
    mongo --host $MONGODB_HOST 10pingdb.js
    if (( $? == 0 )); then
        break
    fi
    sleep 1
done

echo "===Current directory is:" `pwd`

if [ $# -eq 0 ]; then
  echo "No parameter supplied."
  echo "Usage: $0 <collection name>"
else
    mongoexport -d ctafmd -c $1 --jsonArray --pretty -o testdump/$(date -d "today" +"%Y%m%d%H%M")_$1.json
fi


#for i in users areas chorg contactinfotype products

echo "===Export done."